package data.hullmods;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.ShipAPI;
import java.util.HashSet;
import java.util.Set;
import com.fs.starfarer.api.combat.BaseHullMod;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import org.lwjgl.util.vector.Vector2f;

public class KH_engineering extends BaseHullMod {
    
    private static final Set<String> BLOCKED_HULLMODS = new HashSet<>();
    static
    {
        // These hullmods will automatically be removed
        // This prevents unexplained hullmod blocking
        BLOCKED_HULLMODS.add("augmentedengines");
        BLOCKED_HULLMODS.add("auxiliarythrusters");
        BLOCKED_HULLMODS.add("unstable_injector");
        BLOCKED_HULLMODS.add("drive_shunt");   
        BLOCKED_HULLMODS.add("cargo_expansion");   
        BLOCKED_HULLMODS.add("additional_crew_quarters");   
        BLOCKED_HULLMODS.add("supply_conservation_program");   
        BLOCKED_HULLMODS.add("maximized_ordinance");   
        BLOCKED_HULLMODS.add("fuel_expansion");   
        BLOCKED_HULLMODS.add("shieldbypass");   
        BLOCKED_HULLMODS.add("frontshield");   
        BLOCKED_HULLMODS.add("converted_hangar");
    }
    private String ERROR="IncompatibleHullmodWarning";
    private float BEAM_DEBUFF=0.85f;
    private float BEAM_PD=0.95f;

    @Override
    public void applyEffectsBeforeShipCreation(HullSize hullSize, MutableShipStatsAPI stats, String id) {
        if(stats.getVariant().getModuleSlots().isEmpty()){
            stats.getBeamWeaponRangeBonus().modifyMult(id, BEAM_DEBUFF);
            stats.getBeamPDWeaponRangeBonus().modifyMult(id, (1/BEAM_DEBUFF)*BEAM_PD);
        }
    }
        
    @Override
    public void applyEffectsAfterShipCreation(ShipAPI ship, String id){
        
        if(ship.getSystem().getId().equals("kh_module")){        
            for (String tmp : BLOCKED_HULLMODS) {
                if (ship.getVariant().getHullMods().contains(tmp)) {                
                    ship.getVariant().removeMod(tmp);      
                    ship.getVariant().addMod(ERROR);
                }
            }
        }
    }

    @Override
    public String getDescriptionParam(int index, ShipAPI.HullSize hullSize) {
        if (index == 0) return "completely destroyed";
        if (index == 1) return "disabled";
        if (index == 2) return "Makeshift Shield Generators";
        if (index == 3) return "Converted Hangars";
        if (index == 4) return Math.round((1-BEAM_DEBUFF)*100)+"%";
        if (index == 5) return Math.round((1-BEAM_PD)*100)+"%";
        return null;
    }

    //FORCED DEATH

    @Override
    public void advanceInCombat(ShipAPI ship, float amount) {
        if(ship.isAlive() && ship.getParentStation()==null && ship.getVariant().getModuleSlots()!=null){
            int modules = 0;
            int slots = ship.getVariant().getModuleSlots().size();
            
            if(ship.getChildModulesCopy()!=null){
                for(ShipAPI s : ship.getChildModulesCopy()){
                    if(s.isAlive() || (Global.getCombatEngine().isEntityInPlay(s)&&!s.isPiece())){
                        slots--;
                    }
                    if(s.isAlive()){
                        modules++;
                    }
                }
            
                if(slots>0 || modules==0){
                    Global.getCombatEngine().applyDamage(ship, ship.getLocation(), ship.getHitpoints()*2, DamageType.HIGH_EXPLOSIVE, 0, true, false, ship);
                }
            } else {
                Global.getCombatEngine().applyDamage(ship, new Vector2f(), ship.getHitpoints()*2, DamageType.HIGH_EXPLOSIVE, 0, true, false, ship);
            }
        }
    }
}
