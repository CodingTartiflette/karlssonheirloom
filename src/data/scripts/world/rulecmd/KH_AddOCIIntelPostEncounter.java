package data.scripts.world.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.impl.campaign.ids.Ranks;
import com.fs.starfarer.api.impl.campaign.rulecmd.BaseCommandPlugin;
import com.fs.starfarer.api.util.Misc.Token;
import data.campaign.ids.KH_Factions;
import data.campaign.ids.KH_Industries;
import data.scripts.world.events.KH_EventManager;
import data.scripts.world.intel.KH_OCICentralIntel;

import java.util.List;
import java.util.Map;

public class KH_AddOCIIntelPostEncounter extends BaseCommandPlugin
{
    private static void addCentralIntel()
    {
        for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy())
        {
            if (market.hasIndustry(KH_Industries.CENTRAL_STATION))
            {
                new KH_OCICentralIntel(market.getPrimaryEntity(), KH_EventManager.getPersonWithPost(Ranks.POST_STATION_COMMANDER, market));
                return;
            }
        }

        Global.getLogger(KH_AddOCIIntelPostEncounter.class).error("OCI Central station not found!");
    }

    public static void setupOCIIntel()
    {
        // Reveal OCI faction in the Intel Panel
        Global.getSector().getFaction(KH_Factions.OCI).setShowInIntelTab(true);

        // Add intel pointer to Central's commander
        addCentralIntel();

        // TODO: Add OCI timeout tracker once associated quest is in
    }

    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Token> params, Map<String, MemoryAPI> memoryMap)
    {
        setupOCIIntel();
        return true;
    }
}
