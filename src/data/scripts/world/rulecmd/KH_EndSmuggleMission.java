package data.scripts.world.rulecmd;

import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.rulecmd.BaseCommandPlugin;
import com.fs.starfarer.api.util.Misc.Token;
import data.campaign.ids.KH_Entities;
import data.campaign.ids.KH_MemTags;
import data.scripts.world.events.KH_EventManager;
import data.scripts.world.events.KH_EventManager.QuestString;
import data.scripts.world.intel.KH_OCIRumorSpecificIntel;

import java.util.List;
import java.util.Map;

public class KH_EndSmuggleMission extends BaseCommandPlugin
{
    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Token> params, Map<String, MemoryAPI> memoryMap)
    {
        final PersonAPI person = (PersonAPI) memoryMap.get(MemKeys.GLOBAL).get(KH_MemTags.MEM_SMUGGLER_MISSION_AGENT);
        if (person != null) dialog.getVisualPanel().showPersonInfo(person, true);

        final QuestString questString = KH_EventManager.getQuestString("oci_rumor_complete_smuggle");
        questString.addText(dialog);

        memoryMap.get(MemKeys.GLOBAL).unset(KH_MemTags.MEM_SMUGGLE_MISSION_ACTIVE);
        memoryMap.get(MemKeys.GLOBAL).unset(KH_MemTags.MEM_SMUGGLER_MISSION_AGENT);

        new KH_OCIRumorSpecificIntel(KH_EventManager.getNearestOCIBaseOfType(KH_Entities.SMALL_STATION));
        return true;
    }
}
