package data.scripts.world.rulecmd;

import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.rulecmd.BaseCommandPlugin;
import com.fs.starfarer.api.util.Misc.Token;
import data.scripts.world.events.KH_EventManager;

import java.util.List;
import java.util.Map;

public class KH_SetActivePersonUsingPost extends BaseCommandPlugin
{
    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Token> params, Map<String, MemoryAPI> memoryMap)
    {
        if (dialog == null) return false;

        final SectorEntityToken target = dialog.getInteractionTarget();
        if (target == null || target.getMarket() == null) return false;

        final String rank = params.get(0).getString(memoryMap);
        final PersonAPI person = KH_EventManager.getPersonWithPost(rank, target.getMarket());
        if (person == null) return false;

        target.setActivePerson(person);
        return true;
    }
}
