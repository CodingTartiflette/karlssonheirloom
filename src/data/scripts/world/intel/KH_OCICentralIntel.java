package data.scripts.world.intel;

import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;
import data.campaign.ids.KH_Factions;
import data.scripts.world.events.KH_EventManager;

import java.awt.Color;

public class KH_OCICentralIntel extends KH_BasePointerIntel
{
    public KH_OCICentralIntel(SectorEntityToken token, PersonAPI person)
    {
        super(token.getStarSystem(), token, person);
        addIntelTags(KH_Factions.OCI, Tags.INTEL_MISSIONS);
        setRemoveWhenEncountered(true);
    }

    @Override
    public String getSmallDescriptionTitle()
    {
        // This is shown as the title of the right pane when the intel is selected
        return "Talk to " + person.getNameString();
    }

    protected void addBulletPoints(TooltipMakerAPI info, ListInfoMode mode)
    {
        Color h = Misc.getHighlightColor();
        Color g = Misc.getGrayColor();
        float pad = 3f;
        float opad = 10f;

        float initPad = pad;
        if (mode == ListInfoMode.IN_DESC) initPad = opad;

        Color tc = getBulletColorForMode(mode);

        bullet(info);
        info.addPara("Target: " + person.getNameString() + " at " + token.getFullName(),
                initPad, tc, h, person.getNameString(), token.getFullName());
        info.addPara("Location: " + system.getBaseName(),
                initPad, tc, h, system.getBaseName());
        unindent(info);
    }

    @Override
    public void createIntelInfo(TooltipMakerAPI info, ListInfoMode mode)
    {
        // This is shown as the intel description in the left pane
        info.addPara("Visit OCI Central", getTitleColor(mode), 0f);
        addBulletPoints(info, mode);
    }

    @Override
    public void createSmallDescription(TooltipMakerAPI info, float width, float height)
    {
        // This is shown in the right pane when the intel is selected
        KH_EventManager.getQuestString("oci_intel_central_pointer").addTooltip(info, 0f);
    }
}
