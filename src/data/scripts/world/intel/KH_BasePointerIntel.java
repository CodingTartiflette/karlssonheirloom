package data.scripts.world.intel;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.intel.BaseIntelPlugin;
import com.fs.starfarer.api.ui.SectorMapAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Simple intel that points at a system, location, or person, and is removed once they are interacted with.
 */
public abstract class KH_BasePointerIntel extends BaseIntelPlugin
{
    protected CampaignEventListener listener = new PointerListener();
    protected StarSystemAPI system;
    protected SectorEntityToken token;
    protected PersonAPI person;
    protected boolean removeWhenEncountered = false;
    protected Set<String> extraTags = null;

    public KH_BasePointerIntel(StarSystemAPI system, @Nullable SectorEntityToken token, @Nullable PersonAPI person)
    {
        this.system = system;
        this.token = token;
        this.person = person;

        // Add exclamation point to person
        if (person != null){
            person.incrWantsToContactReasons();
            Misc.makeImportant(person, "oci");
        }

        Global.getSector().addScript(this);
        Global.getSector().addListener(listener);
        Global.getSector().getIntelManager().addIntel(this);
    }

    @Override
    protected void advanceImpl(float amount)
    {
        super.advanceImpl(amount);

        if (removeWhenEncountered)
        {
            // Person intel - remove when talked to
            if (person != null)
            {
                if (!person.wantsToContactPlayer())
                {
                    endImmediately();
                }
            }
            // System intel - remove when inside system
            else if (token == null && Global.getSector().getCurrentLocation() == system)
            {
                endImmediately();
            }
        }
    }

    public void addIntelTags(String... tags)
    {
        if (extraTags == null) extraTags = new HashSet<>();
        extraTags.addAll(Arrays.asList(tags));
    }

    public void removeIntelTags(String... tags)
    {
        if (extraTags != null) extraTags.removeAll(Arrays.asList(tags));
    }

    public void setRemoveWhenEncountered(boolean removeWhenEncountered)
    {
        this.removeWhenEncountered = removeWhenEncountered;
    }

    @Override
    public boolean canMakeVisibleToPlayer(boolean playerInRelayRange)
    {
        return true;
    }

    @Override
    protected void notifyEnded()
    {
        super.notifyEnded();
        Global.getSector().removeScript(this);
        Global.getSector().removeListener(listener);
    }

    @Override
    public Set<String> getIntelTags(SectorMapAPI map)
    {
        final Set<String> tags = super.getIntelTags(map);
        if (extraTags != null) tags.addAll(extraTags);
        return tags;
    }

    @Override
    public String getIcon()
    {
        return Global.getSettings().getSpriteName("intel", "discovered_entity");
    }

    @Override
    public SectorEntityToken getMapLocation(SectorMapAPI map)
    {
        return (token == null ? system.getCenter() : token);
    }

    @Override
    public abstract String getSmallDescriptionTitle();

    @Override
    public abstract void createIntelInfo(TooltipMakerAPI info, ListInfoMode mode);

    @Override
    public abstract void createSmallDescription(TooltipMakerAPI info, float width, float height);

    private class PointerListener extends BaseCampaignEventListener
    {
        private PointerListener()
        {
            super(false);
            Global.getSector().addListener(this);
        }

        @Override
        public void reportShownInteractionDialog(InteractionDialogAPI dialog)
        {
            if (!removeWhenEncountered) return;

            // End token intel when interacted with
            if (person == null && token != null && dialog.getInteractionTarget() == token)
            {
                endImmediately();
            }
        }
    }
}
