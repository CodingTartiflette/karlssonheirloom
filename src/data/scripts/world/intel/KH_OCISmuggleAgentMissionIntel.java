package data.scripts.world.intel;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.characters.FullName.Gender;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.impl.campaign.intel.BaseIntelPlugin;
import com.fs.starfarer.api.ui.SectorMapAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;
import data.campaign.ids.KH_MemTags;

import java.awt.Color;
import java.util.Set;

public class KH_OCISmuggleAgentMissionIntel extends BaseIntelPlugin
{
    private final PersonAPI agent;
    private float nextCheck = 0f;

    public KH_OCISmuggleAgentMissionIntel(PersonAPI agent)
    {
        this.agent = agent;
        Global.getSector().addScript(this);
        Global.getSector().getIntelManager().addIntel(this);
    }

    @Override
    public void advanceImpl(float amount)
    {
        nextCheck -= amount;
        if (nextCheck <= 0f)
        {
            nextCheck = 1f;

            // Remove intel once agent is successfully smuggled to a Tri-Tachyon base
            if (!Global.getSector().getMemoryWithoutUpdate().getBoolean(KH_MemTags.MEM_SMUGGLE_MISSION_ACTIVE))
            {
                endAfterDelay(1f);
            }
        }
    }

    @Override
    public Set<String> getIntelTags(SectorMapAPI map)
    {
        final Set<String> tags = super.getIntelTags(map);
        tags.add(Tags.INTEL_MISSIONS);
        return tags;
    }

    @Override
    public String getSmallDescriptionTitle()
    {
        return "Smuggle Agent";
    }

    protected void addBulletPoints(TooltipMakerAPI info, ListInfoMode mode)
    {
        Color h = Misc.getHighlightColor();
        Color g = Misc.getGrayColor();
        float pad = 3f;
        float opad = 10f;

        float initPad = pad;
        if (mode == ListInfoMode.IN_DESC) initPad = opad;

        Color tc = getBulletColorForMode(mode);
        final FactionAPI tritach = Global.getSector().getFaction(Factions.TRITACHYON);

        bullet(info);
        info.addPara("Target: any " + tritach.getDisplayName() + " station", initPad, tc,
                tritach.getBaseUIColor(), tritach.getDisplayName());
        unindent(info);
    }

    @Override
    public void createIntelInfo(TooltipMakerAPI info, ListInfoMode mode)
    {
        info.addPara("Smuggle Agent", getTitleColor(mode), 0f);
        addBulletPoints(info, mode);
    }

    @Override
    public void createSmallDescription(TooltipMakerAPI info, float width, float height)
    {
        info.addPara("You've agreed to smuggle " + agent.getNameString()
                + " to a friendly Tri-Tachyon station in return for information on a cache of equipment "
                + (agent.getGender() == Gender.FEMALE ? "she" : "he") + " claims is guarded by \"Flying Saucers\".", 0f);
    }
}
