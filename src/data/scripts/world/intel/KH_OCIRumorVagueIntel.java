package data.scripts.world.intel;

import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;
import data.campaign.ids.KH_Factions;
import data.scripts.world.events.KH_EventManager;

import java.awt.Color;

public class KH_OCIRumorVagueIntel extends KH_BasePointerIntel
{
    public KH_OCIRumorVagueIntel(StarSystemAPI system)
    {
        super(system, null, null);
        addIntelTags(KH_Factions.OCI, Tags.INTEL_EXPLORATION);
        setRemoveWhenEncountered(true);
    }

    @Override
    public String getSmallDescriptionTitle()
    {
        return "Point of Interest";
    }

    protected void addBulletPoints(TooltipMakerAPI info, ListInfoMode mode)
    {
        Color h = Misc.getHighlightColor();
        Color g = Misc.getGrayColor();
        float pad = 3f;
        float opad = 10f;

        float initPad = pad;
        if (mode == ListInfoMode.IN_DESC) initPad = opad;

        Color tc = getBulletColorForMode(mode);

        bullet(info);
        info.addPara("Location: " + system.getBaseName(),
                initPad, tc, h, system.getBaseName());
        unindent(info);
    }

    @Override
    public void createIntelInfo(TooltipMakerAPI info, ListInfoMode mode)
    {
        info.addPara("Point of Interest", getTitleColor(mode), 0f);
        addBulletPoints(info, mode);
    }

    @Override
    public void createSmallDescription(TooltipMakerAPI info, float width, float height)
    {
        KH_EventManager.getQuestString("oci_intel_system_pointer").addTooltip(info, 0f);
    }
}
