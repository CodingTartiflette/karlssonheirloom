package data.scripts.world.events;

import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.OptionPanelAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.impl.campaign.intel.bar.PortsideBarEvent;
import com.fs.starfarer.api.impl.campaign.intel.bar.events.BarEventManager;
import com.fs.starfarer.api.impl.campaign.intel.bar.events.BaseBarEventCreator;
import com.fs.starfarer.api.util.Misc;
import data.campaign.ids.KH_Factions;

import java.util.EnumSet;
import java.util.Map;

public class KH_OCIBarStoriesEvent extends KH_BaseBarEvent
{
    private transient int storyStage = 1; // Used to control which text is shown during exposition
    private int currentPage = 1;
    private int ociLore = 0;
    private EnumSet<Story> heardStories = EnumSet.noneOf(Story.class);
    private EnumSet<Article> readArticles = EnumSet.noneOf(Article.class);

    private KH_OCIBarStoriesEvent()
    {
        super();
    }

    @Override
    public boolean shouldShowAtMarket(MarketAPI market)
    {
        // Available at all OCI bars simultaneously
        return KH_Factions.OCI.equals(market.getFactionId());
    }

    @Override
    public boolean shouldRemoveEvent()
    {
        return false;
    }

    @Override
    protected String getPersonFaction()
    {
        return KH_Factions.OCI;
    }

    private enum Option
    {
        INIT, // Start of conversation, chooses stories available
        LEAVE
    }

    private enum Story
    {
        STORY_1("oci_bar_story_1", 0),
        STORY_2("oci_bar_story_2", 0),
        STORY_3("oci_bar_story_3", 0),
        STORY_4("oci_bar_story_4", 0),
        STORY_5("oci_bar_story_5", 3),
        STORY_6("oci_bar_story_6", 3),
        STORY_7("oci_bar_story_7", 3),
        STORY_8("oci_bar_story_8", 4),
        STORY_9("oci_bar_story_9", 4);

        private final String questStringId;
        private final int requiredLore;

        Story(String questStringId, int requiredLore)
        {
            this.questStringId = questStringId;
            this.requiredLore = requiredLore;
        }

        private boolean hasText(int stage)
        {
            return KH_EventManager.hasQuestString(questStringId + "_" + stage);
        }

        private void addText(int stage, InteractionDialogAPI dialog)
        {
            KH_EventManager.getQuestString(questStringId + "_" + stage).addText(dialog);
        }

        private boolean hasOption(int stage)
        {
            return KH_EventManager.hasQuestString(questStringId + (stage == 0 ? "_start" : "_continue_" + stage));
        }

        private void addOption(int stage, InteractionDialogAPI dialog)
        {
            KH_EventManager.getQuestString(questStringId + (stage == 0 ? "_start" : "_continue_" + stage)).addOption(dialog, this);
        }
    }

    private enum Article
    {
        ARTICLE_1("oci_bar_article_1"),
        ARTICLE_2("oci_bar_article_2"),
        ARTICLE_3("oci_bar_article_3"),
        ARTICLE_4("oci_bar_article_4");

        private final String questStringId;

        private Article(String questStringId)
        {
            this.questStringId = questStringId;
        }

        private boolean hasText(int stage)
        {
            return KH_EventManager.hasQuestString(questStringId + "_" + stage);
        }

        private void addText(int page, InteractionDialogAPI dialog)
        {
            KH_EventManager.getQuestString(questStringId + "_" + page).addText(dialog);
        }

        private boolean hasOption(int page)
        {
            return KH_EventManager.hasQuestString(questStringId + (page == 0 ? "_start" : "_continue_" + page));
        }

        private void addOption(int page, InteractionDialogAPI dialog)
        {
            KH_EventManager.getQuestString(questStringId + (page == 0 ? "_start" : "_continue_" + page)).addOption(dialog, this);
        }
    }

    @Override
    public void addPromptAndOption(InteractionDialogAPI dialog)
    {
        super.addPromptAndOption(dialog);

        final SectorEntityToken target = dialog.getInteractionTarget();
        regen(target.getMarket());
        target.setActivePerson(getPerson());

        addText("oci_bar_start", dialog);
        addOption("oci_bar_take_seat", dialog, this);
    }

    @Override
    public void init(InteractionDialogAPI dialog)
    {
        super.init(dialog);

        // If all content has been exhausted, end the event
        if (heardStories.size() == Story.values().length && readArticles.size() == Article.values().length)
        {
            BarEventManager.getInstance().notifyWasInteractedWith(this);
            done = true;
            noContinue = true;
            return;
        }

        done = false;
        storyStage = 1;

        dialog.getVisualPanel().showPersonInfo(person, true);
        optionSelected(null, Option.INIT);
    }

    @Override
    public void optionSelected(String optionText, Object optionData)
    {
        if (!(optionData instanceof Option) && !(optionData instanceof Story) && !(optionData instanceof Article))
        {
            return;
        }

        final OptionPanelAPI options = dialog.getOptionPanel();
        final Map<String, MemoryAPI> memoryMap = dialog.getPlugin().getMemoryMap();
        final MemoryAPI localMem = memoryMap.get(MemKeys.LOCAL);
        options.clearOptions();

        if (optionData instanceof Option)
        {
            switch ((Option) optionData)
            {
                // Generate available stories based on what you've heard already this session
                case INIT:
                    storyStage = 1;

                    // If you have a commission with OCI's enemies, they give a lower chance of you being able to help them
                    localMem.set("$helpOCIPercent", (KH_OCIRumorEvent.isHegemonyOrChurch(Misc.getCommissionFactionId())
                            ? "fifty three point seven" : "seventy eight point three"), 0f);

                    // Add any unheard stories to the
                    for (Story story : Story.values())
                    {
                        if (!heardStories.contains(story) && ociLore >= story.requiredLore)
                        {
                            story.addOption(0, dialog);
                        }
                    }

                    if (ociLore > 5 && currentPage < 5)
                    {
                        Article.values()[currentPage - 1].addOption(0, dialog);
                    }

                    options.addOption("Leave", Option.LEAVE);
                    break;
                case LEAVE:
                    noContinue = true;
                    done = true;
                    break;
            }
        }
        else if (optionData instanceof Story)
        {
            final Story story = (Story) optionData;
            if (story.hasText(storyStage))
            {
                story.addText(storyStage, dialog);
            }

            if (story.hasOption(storyStage))
            {
                story.addOption(storyStage, dialog);
                storyStage++;
            }
            else
            {
                heardStories.add(story);
                ociLore++;
                done = true;
                noContinue = true;
                //options.addOption("Continue", Option.INIT);
            }
        }
        else
        {
            final Article article = (Article) optionData;
            if (article.hasText(storyStage))
            {
                article.addText(storyStage, dialog);
            }

            if (article.hasOption(storyStage))
            {
                article.addOption(storyStage, dialog);
                storyStage++;
            }
            else
            {
                readArticles.add(article);
                currentPage++;
                done = true;
                noContinue = true;
                //options.addOption("Continue", Option.INIT);
            }
        }
    }

    static class EventCreator extends BaseBarEventCreator
    {
        public PortsideBarEvent createBarEvent()
        {
            return new KH_OCIBarStoriesEvent();
        }

        @Override
        public float getBarEventAcceptedTimeoutDuration()
        {
            return 60f;
        }

        @Override
        public float getBarEventTimeoutDuration()
        {
            return 60f;
        }

        @Override
        public float getBarEventFrequencyWeight()
        {
            return 9999f; // Always available at OCI bars
        }

        @Override
        public boolean isPriority()
        {
            return true;
        }
    }
}
