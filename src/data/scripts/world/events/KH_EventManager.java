package data.scripts.world.events;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.CommDirectoryEntryAPI.EntryType;
import com.fs.starfarer.api.campaign.PersistentUIDataAPI.AbilitySlotAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.impl.campaign.intel.bar.events.BarEventManager;
import com.fs.starfarer.api.loading.AbilitySpecAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;
import data.campaign.ids.KH_Abilities;
import data.campaign.ids.KH_Factions;
import data.campaign.ids.KH_MemTags;
import data.campaign.ids.KH_Tags;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONObject;
import org.lazywizard.lazylib.JSONUtils;

import java.awt.Color;
import java.util.*;

public class KH_EventManager
{
    private static final String QUEST_STRING_CSV = "data/strings/kh_event_strings.csv";
    private static Map<String, QuestString> questStrings = null;

    public static void updateAvailableEvents()
    {
        final BarEventManager bar = BarEventManager.getInstance();
        if (!bar.hasEventCreator(KH_OCIRumorEvent.EventCreator.class))
        {
            bar.addEventCreator(new KH_OCIRumorEvent.EventCreator());
        }
        if (!bar.hasEventCreator(KH_OCIBarStoriesEvent.EventCreator.class))
        {
            bar.addEventCreator(new KH_OCIBarStoriesEvent.EventCreator());
        }
    }


    @Nullable
    public static PersonAPI getPersonWithPost(String postId, MarketAPI market)
    {
        for (CommDirectoryEntryAPI entry : market.getCommDirectory().getEntriesCopy())
        {
            if (entry.getType() == EntryType.PERSON)
            {
                final PersonAPI person = (PersonAPI) entry.getEntryData();
                if (postId.equalsIgnoreCase(person.getPostId())) return person;
            }
        }

        return null;
    }

    public static StarSystemAPI getNearestOCISystem()
    {
        StarSystemAPI nearest = null;
        float nearestDist = Float.MAX_VALUE;
        for (StarSystemAPI system : Global.getSector().getStarSystems())
        {
            if (system.hasTag(KH_Tags.THEME_OCI))
            {
                final float dist = Misc.getDistanceToPlayerLY(system.getLocation());
                if (dist < nearestDist)
                {
                    nearest = system;
                    nearestDist = dist;
                }
            }
        }

        return nearest;
    }

    public static SectorEntityToken getNearestOCIBaseOfType(String stationType)
    {
        final List<StarSystemAPI> ociSystems = new ArrayList<>();
        for (StarSystemAPI system : Global.getSector().getStarSystems())
        {
            if (system.getTags().contains(KH_Tags.THEME_OCI))
            {
                ociSystems.add(system);
            }
        }

        Collections.sort(ociSystems, new Comparator<StarSystemAPI>()
        {
            @Override
            public int compare(StarSystemAPI o1, StarSystemAPI o2)
            {
                return Float.compare(Misc.getDistanceToPlayerLY(o1.getLocation()),
                        Misc.getDistanceToPlayerLY(o2.getLocation()));
            }
        });

        for (StarSystemAPI system : ociSystems)
        {
            final List<CustomCampaignEntityAPI> stations = system.getCustomEntitiesWithTag(Tags.STATION);
            for (SectorEntityToken token : stations)
            {
                if (stationType.equals(token.getCustomEntityType())) return token;
            }

            if (!stations.isEmpty()) return stations.get(0);
        }

        return null;
    }

    public static boolean hasPlayerEncounteredOCI()
    {
        return Global.getSector().getFaction(KH_Factions.OCI)
                .getMemoryWithoutUpdate().getBoolean(KH_MemTags.MEM_ENCOUNTERED_OCI);
    }

    public static void setPlayerEncounteredOCI(boolean hasEncountered)
    {
        Global.getSector().getFaction(KH_Factions.OCI)
                .getMemoryWithoutUpdate().set(KH_MemTags.MEM_ENCOUNTERED_OCI, hasEncountered);
    }

    public static void unlockSlipstreamAbility()
    {
        final CharacterDataAPI player = Global.getSector().getCharacterData();
        if (player.getAbilities().contains(KH_Abilities.SLIPSTREAM_DRIVE)) return;

        // Unlock the ability and add it to the first available hotbar slot
        player.addAbility(KH_Abilities.SLIPSTREAM_DRIVE);
        for (AbilitySlotAPI slot : Global.getSector().getUIData().getAbilitySlotsAPI().getCurrSlotsCopy())
        {
            if (slot.getAbilityId() == null)
            {
                slot.setAbilityId(KH_Abilities.SLIPSTREAM_DRIVE);
                break;
            }
        }

        // Notify the player of their new ability
        final AbilitySpecAPI ability = Global.getSettings().getAbilitySpec(KH_Abilities.SLIPSTREAM_DRIVE);
        final CampaignUIAPI ui = Global.getSector().getCampaignUI();
        final String str = "\"" + ability.getName() + "\"";
        getQuestString("oci_slipstream_unlock_notification").addText(ui);
        ui.addMessage("Gained ability: " + str + "", Misc.getPositiveHighlightColor(),
                str, "", Misc.getHighlightColor(), Misc.getHighlightColor());
    }

    private static void reloadQuestStrings()
    {
        try
        {
            questStrings = new HashMap<>();
            final JSONArray csv = Global.getSettings().loadCSV(QUEST_STRING_CSV);
            for (int i = 0; i < csv.length(); i++)
            {
                final JSONObject row = csv.getJSONObject(i);
                final String id = row.optString("id", null);
                if (id == null || id.isEmpty()) continue;

                final String text = row.getString("text").replace("\\n", "\n"),
                        highlightsRaw = row.optString("highlights", ""),
                        baseColorRaw = row.optString("baseColor", ""),
                        highlightColorOverride = row.optString("highlightColorOverride", "")
                                .replace('{', '[').replace('}', ']');
                final String[] highlights = (highlightsRaw.isEmpty() ? null : highlightsRaw.split(";"));
                final Color baseColor = (baseColorRaw.isEmpty() ? null : JSONUtils.toColor(new JSONArray(baseColorRaw))),
                        highlightColor = (highlightColorOverride.isEmpty() ? Misc.getHighlightColor()
                                : JSONUtils.toColor(new JSONArray(highlightColorOverride)));

                questStrings.put(id, new QuestString(text, baseColor, highlightColor, highlights));
            }
        }
        catch (Exception ex)
        {
            throw new RuntimeException("Failed to load quest strings!", ex);
        }
    }

    public static boolean hasQuestString(String id)
    {
        if (questStrings == null) reloadQuestStrings();
        return questStrings.containsKey(id);
    }

    /**
     * Retrieves a loaded quest string.
     *
     * @param id The ID of the quest string in {@link #QUEST_STRING_CSV}.
     *
     * @return A {@link QuestString}, which handles formatting and coloring the String as well as adding it to the
     *         {@link InteractionDialogAPI}.
     */
    public static QuestString getQuestString(String id)
    {
        if (questStrings == null) reloadQuestStrings();

        final QuestString questString = questStrings.get(id);
        if (questString == null)
        {
            return new QuestString("{ERROR: no quest string found with id '" + id + "'}");
        }

        return questString;
    }

    public static class QuestString
    {
        private final String baseText;
        private final String[] highlights;
        private final Color baseColor, highlightColor;

        public QuestString(String text, Color baseColor, Color highlightColor, String... highlights)
        {
            baseText = text;
            this.baseColor = baseColor;
            this.highlights = highlights;
            this.highlightColor = highlightColor;
        }

        public QuestString(String text)
        {
            this(text, null, null);
        }

        private static String tokenize(String toTokenize)
        {
            return Misc.getStringWithTokenReplacement(toTokenize, Global.getSector().getPlayerFleet(), null);
        }

        private static String tokenize(String toTokenize, InteractionDialogAPI dialog)
        {
            return Misc.getStringWithTokenReplacement(toTokenize,
                    dialog.getInteractionTarget(), dialog.getPlugin().getMemoryMap());
        }

        private static String[] tokenize(String[] toTokenize, InteractionDialogAPI dialog)
        {
            final String[] tokenized = new String[toTokenize.length];
            for (int i = 0; i < toTokenize.length; i++)
            {
                tokenized[i] = tokenize(toTokenize[i], dialog);
            }

            return tokenized;
        }

        public void addText(CampaignUIAPI ui)
        {
            // TODO: Handle highlights
            ui.addMessage(tokenize(baseText), (baseColor == null ? Misc.getTextColor() : baseColor));
        }

        public void addText(InteractionDialogAPI dialog)
        {
            final TextPanelAPI text = dialog.getTextPanel();

            if (baseColor != null)
            {
                text.addPara(tokenize(baseText, dialog), baseColor);
            }
            else
            {
                text.addPara(tokenize(baseText, dialog));
            }

            if (highlights != null && highlights.length > 0)
            {
                text.highlightInLastPara(highlightColor, tokenize(highlights, dialog));
            }
        }

        public void addOption(InteractionDialogAPI dialog, Object optionData)
        {
            final OptionPanelAPI options = dialog.getOptionPanel();

            if (baseColor != null)
            {
                options.addOption(tokenize(baseText, dialog), optionData, baseColor, null);
            }
            else
            {
                options.addOption(tokenize(baseText, dialog), optionData);
            }

            if (highlights != null && highlights.length > 0)
            {
                options.setTooltipHighlightColors(optionData, highlightColor);
                options.setTooltipHighlights(optionData, tokenize(highlights, dialog));
            }
        }

        public void addTooltip(TooltipMakerAPI info, float padding)
        {
            // TODO: Handle highlights
            if (baseColor != null)
            {
                info.addPara(tokenize(baseText), baseColor, padding);
            }
            else
            {
                info.addPara(tokenize(baseText), padding);
            }
        }
    }

    private KH_EventManager()
    {
    }
}
