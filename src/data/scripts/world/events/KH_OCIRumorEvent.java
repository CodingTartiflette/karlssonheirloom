package data.scripts.world.events;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.OptionPanelAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.intel.bar.PortsideBarEvent;
import com.fs.starfarer.api.impl.campaign.intel.bar.events.BarEventManager;
import com.fs.starfarer.api.impl.campaign.intel.bar.events.BaseBarEventCreator;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.MutableValue;
import data.campaign.ids.KH_Entities;
import data.campaign.ids.KH_MemTags;
import data.scripts.world.intel.KH_OCIRumorSpecificIntel;
import data.scripts.world.intel.KH_OCIRumorVagueIntel;
import data.scripts.world.intel.KH_OCISmuggleAgentMissionIntel;

import java.text.DecimalFormat;

class KH_OCIRumorEvent extends KH_BaseBarEvent
{
    private static final float WIRE_COST = 100_000f; // Cost to get the better intel without doing a mission for it
    private int storyStage = 1; // Used to control which text is shown during exposition
    private boolean refused = false; // Used to control which dialog is shown on return events

    private KH_OCIRumorEvent()
    {
        super();
    }

    static boolean isHegemonyOrChurch(String factionId)
    {
        if (factionId == null) return false;
        return Factions.HEGEMONY.equals(factionId) || Factions.LUDDIC_CHURCH.equals(factionId);
    }

    @Override
    public boolean shouldShowAtMarket(MarketAPI market)
    {
        if (!super.shouldShowAtMarket(market)) return false;

        // If the player has already encountered OCI, don't spawn the agent
        if (KH_EventManager.hasPlayerEncounteredOCI()) return false;

        // Don't show if the player (somehow) already has the smuggling mission active
        if (Global.getSector().getMemoryWithoutUpdate().getBoolean(KH_MemTags.MEM_SMUGGLE_MISSION_ACTIVE)) return false;

        // The agent only appears at Hegemony or Luddic Church markets
        return isHegemonyOrChurch(market.getFactionId());
    }

    @Override
    public boolean shouldRemoveEvent()
    {
        // Remove event if player has encountered OCI, as the "reward" becomes meaningless
        return KH_EventManager.hasPlayerEncounteredOCI();
    }

    // The various dialog options
    private enum Option
    {
        INIT, // Start of conversation, chooses branching story paths
        CONTINUE, // Agent tells story
        ACCEPT_SMUGGLE, // Smuggle agent to friendly station for information
        ACCEPT_WIRE, // Pay 100k for information
        REFUSE, // Refuse above two offers - will be reoffered when player returns
        LEAVE // Only shown when agent flees; permanently abandons quest
    }

    @Override
    public void addPromptAndOption(InteractionDialogAPI dialog)
    {
        super.addPromptAndOption(dialog);

        final SectorEntityToken target = dialog.getInteractionTarget();
        regen(target.getMarket());
        target.setActivePerson(getPerson());

        addText(refused ? "oci_rumor_repeat" : "oci_rumor_start", dialog);
        addOption(refused ? "oci_rumor_repeat_take_seat" : "oci_rumor_take_seat", dialog, this);
    }

    @Override
    public void init(InteractionDialogAPI dialog)
    {
        super.init(dialog);

        done = false;

        dialog.getVisualPanel().showPersonInfo(person, true);
        optionSelected(null, Option.INIT);
    }

    private void checkDisableWireOption(MutableValue credits, OptionPanelAPI options)
    {
        if (credits.get() < WIRE_COST)
        {
            final String needed = DecimalFormat.getIntegerInstance().format(Math.round(WIRE_COST - credits.get()));
            options.setEnabled(Option.ACCEPT_WIRE, false);
            options.setTooltip(Option.ACCEPT_WIRE, "You don't have enough credits (need "
                    + needed + " more).");
            options.setTooltipHighlightColors(Option.ACCEPT_WIRE, Misc.getHighlightColor());
            options.setTooltipHighlights(Option.ACCEPT_WIRE, needed);
        }
        else
        {
            final String total = DecimalFormat.getIntegerInstance().format(credits.get());
            options.setTooltip(Option.ACCEPT_WIRE, "This will cost " + (int) WIRE_COST
                    + " credits. You have " + total + ".");
            options.setTooltipHighlightColors(Option.ACCEPT_WIRE, Misc.getHighlightColor());
            options.setTooltipHighlights(Option.ACCEPT_WIRE, "" + WIRE_COST, total);
        }
    }

    // Points at nearest OCI-themed system
    private void addSystemIntel()
    {
        new KH_OCIRumorVagueIntel(KH_EventManager.getNearestOCISystem());
    }

    // Points at nearest small OCI base
    private void addStationIntel()
    {
        new KH_OCIRumorSpecificIntel(KH_EventManager.getNearestOCIBaseOfType(KH_Entities.SMALL_STATION));
    }

    private void addSmuggleMission()
    {
        final MemoryAPI mem = dialog.getPlugin().getMemoryMap().get(MemKeys.GLOBAL);
        mem.set(KH_MemTags.MEM_SMUGGLE_MISSION_ACTIVE, true);
        mem.set(KH_MemTags.MEM_SMUGGLER_MISSION_AGENT, person);
        new KH_OCISmuggleAgentMissionIntel(person);
    }

    @Override
    public void optionSelected(String optionText, Object optionData)
    {
        if (!(optionData instanceof Option))
        {
            return;
        }

        final MutableValue credits = Global.getSector().getPlayerFleet().getCargo().getCredits();
        final boolean willFlee = isHegemonyOrChurch(Misc.getCommissionFactionId());
        final OptionPanelAPI options = dialog.getOptionPanel();
        options.clearOptions();
        switch ((Option) optionData)
        {
            // Initial dialog, varies based on commission and whether you've talked to the agent before
            case INIT:
                storyStage = 1;
                // If player is commissioned to a hostile faction, agent flees and bartender gives a lesser rumor
                if (willFlee)
                {
                    dialog.hideVisualPanel(); // Agent runs away
                    addText("oci_rumor_flee_1");
                    addOption("oci_rumor_flee_continue_1", Option.CONTINUE);

                    // Figure out what the nearest OCI system is
                    final StarSystemAPI nearestSystem = KH_EventManager.getNearestOCISystem();
                    if (nearestSystem == null)
                    {
                        // TODO: Handle gracefully and in-character
                        dialog.getTextPanel().addPara("Error: no OCI systems found!");
                        done = true;
                        break;
                    }

                    final String systemName = nearestSystem.getBaseName();
                    dialog.getPlugin().getMemoryMap().get(MemKeys.LOCAL).set("$nearestOCISystem", systemName, 0f);
                }
                // Normal path, player is not aligned with a hostile faction
                else
                {
                    // First meeting, agent shares his story and makes an offer
                    if (!refused)
                    {
                        addText("oci_rumor_offer_1");
                        addOption("oci_rumor_offer_continue_1", Option.CONTINUE);
                    }
                    // On return trips after you've refused once before, agent is drunk and offers you the same choices
                    else
                    {
                        addText("oci_rumor_repeat_offer");
                        addOption("oci_rumor_accept_smuggle", Option.ACCEPT_SMUGGLE);
                        addOption("oci_rumor_accept_wire", Option.ACCEPT_WIRE);
                        //addOption("oci_rumor_refuse", Option.REFUSE);
                        options.addOption("Leave", Option.REFUSE); // REFUSE is temporary, LEAVE is permanent

                        // Disable wire transfer if player can't afford the cost
                        checkDisableWireOption(credits, options);
                    }
                }
                break;
            // Agent tells story
            case CONTINUE:
                storyStage++;

                // Player works for enemy - agent flees, bartender gives lesser rumor
                if (willFlee)
                {
                    addText("oci_rumor_flee_" + storyStage);

                    // Add pointer to nearest OCI-themed system
                    if (storyStage == 4)
                    {
                        addSystemIntel();
                        BarEventManager.getInstance().notifyWasInteractedWith(this);
                        options.addOption("Leave", Option.LEAVE);
                    }
                    else
                    {
                        addOption("oci_rumor_flee_continue_" + storyStage, Option.CONTINUE);
                    }
                }
                // Neutral event - agent offers to share intel if you smuggle them out, or give them money to hire someone else
                else
                {
                    addText("oci_rumor_offer_" + storyStage);
                    if (storyStage >= 5)
                    {
                        addOption("oci_rumor_accept_smuggle", Option.ACCEPT_SMUGGLE);
                        addOption("oci_rumor_accept_wire", Option.ACCEPT_WIRE);
                        addOption("oci_rumor_refuse", Option.REFUSE);

                        // Disable wire transfer if player can't afford the cost
                        checkDisableWireOption(credits, options);
                    }
                    else
                    {
                        addOption("oci_rumor_offer_continue_" + storyStage, Option.CONTINUE);
                    }
                }
                break;
            // Player agrees to smuggle the agent to a nearby Tri-Tachyon station
            case ACCEPT_SMUGGLE:
                addText("oci_rumor_accepted_smuggle");
                addSmuggleMission();
                BarEventManager.getInstance().notifyWasInteractedWith(this);
                done = true;
                break;
            // Player agrees to pay enough for the agent to bribe their own way out
            case ACCEPT_WIRE:
                addText("oci_rumor_accepted_wire");
                credits.subtract(WIRE_COST);
                addStationIntel();
                BarEventManager.getInstance().notifyWasInteractedWith(this);
                done = true;
                break;
            // Player refuses - agent remains, but will be drunk if/when the player returns
            case REFUSE:
                addText("oci_rumor_refused");
                refused = true;
                done = true;
                break;
            case LEAVE:
                //default:
                noContinue = true;
                done = true;
                break;
        }
    }


    static class EventCreator extends BaseBarEventCreator
    {
        public PortsideBarEvent createBarEvent()
        {
            return new KH_OCIRumorEvent();
        }

        @Override
        public float getBarEventAcceptedTimeoutDuration()
        {
            return 10000000000f; // one-time-only
        }

        @Override
        public float getBarEventTimeoutDuration()
        {
            return 0f;
        }

        @Override
        public float getBarEventFrequencyWeight()
        {
            // If the player has already encountered OCI, this rumor would be pointless
            if (KH_EventManager.hasPlayerEncounteredOCI()) return 0f;

            return super.getBarEventFrequencyWeight() * 2f;
        }

        @Override
        // This event is only applicable in the early game, so ensure it shows up ASAP
        public boolean isPriority()
        {
            return true;
        }
    }
}
