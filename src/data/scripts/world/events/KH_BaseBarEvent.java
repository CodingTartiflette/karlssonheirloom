package data.scripts.world.events;

import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.impl.campaign.intel.bar.events.BaseBarEventWithPerson;

// These methods handle loading and formatting text, as well as adding it to the options and text panels
public class KH_BaseBarEvent extends BaseBarEventWithPerson
{
    protected void addText(String questStringId)
    {
        addText(questStringId, dialog);
    }

    protected void addOption(String questStringId, Object optionData)
    {
        addOption(questStringId, dialog, optionData);
    }

    // dialog is not set when addPromptAndOption() is called, so these overloads are needed
    protected void addOption(String questStringId, InteractionDialogAPI dialog, Object optionData)
    {
        KH_EventManager.getQuestString(questStringId).addOption(dialog, optionData);
    }

    protected void addText(String questStringId, InteractionDialogAPI dialog)
    {
        KH_EventManager.getQuestString(questStringId).addText(dialog);
    }
}
