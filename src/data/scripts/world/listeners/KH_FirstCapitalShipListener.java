package data.scripts.world.listeners;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.BaseCampaignEventListener;
import com.fs.starfarer.api.campaign.PlayerMarketTransaction;
import com.fs.starfarer.api.campaign.PlayerMarketTransaction.ShipSaleInfo;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.ShipHullSpecAPI;
import data.campaign.ids.KH_Factions;
import data.scripts.world.events.KH_EventManager;

public class KH_FirstCapitalShipListener extends BaseCampaignEventListener
{
    public KH_FirstCapitalShipListener()
    {
        super(true);
    }

    @Override
    public void reportPlayerMarketTransaction(PlayerMarketTransaction transaction)
    {
        // Unlocks Slipstream ability and removes this listener once the player purchases an OCI capital ship
        final MarketAPI market = transaction.getMarket();
        if (KH_Factions.OCI.equals(market.getFactionId()))
        {
            for (ShipSaleInfo bought : transaction.getShipsBought())
            {
                final ShipHullSpecAPI spec = bought.getMember().getHullSpec();
                if (spec.getBaseHullId().startsWith("kh_") && spec.getHullSize() == HullSize.CAPITAL_SHIP)
                {
                    KH_EventManager.unlockSlipstreamAbility();
                    Global.getSector().removeListener(this);
                }
            }
        }
    }
}
