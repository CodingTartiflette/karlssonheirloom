package data.scripts.world;

import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorGeneratorPlugin;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.procgen.themes.SectorThemeGenerator;
import data.campaign.ids.KH_Factions;
import data.campaign.procgen.themes.KH_ThemeGenerator;

public class OCIGen implements SectorGeneratorPlugin
{

    @Override
    public void generate(SectorAPI sector){
        SectorThemeGenerator.generators.add(1, new KH_ThemeGenerator());
        initFactionRelationships(sector);
    }

    public static void initFactionRelationships(SectorAPI sector){
        FactionAPI oci = sector.getFaction(KH_Factions.OCI);
        
        for(FactionAPI f : sector.getAllFactions()){
            oci.setRelationship(f.getId(), RepLevel.FAVORABLE);
        }
        
        oci.setRelationship(Factions.HEGEMONY, RepLevel.INHOSPITABLE);
        oci.setRelationship(Factions.LUDDIC_CHURCH, RepLevel.INHOSPITABLE);
        oci.setRelationship(Factions.LUDDIC_PATH, RepLevel.INHOSPITABLE);
        oci.setRelationship(Factions.PIRATES, RepLevel.INHOSPITABLE);
    }
}