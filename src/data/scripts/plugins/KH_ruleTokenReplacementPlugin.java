package data.scripts.plugins;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.campaign.rules.RuleTokenReplacementGeneratorPlugin;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.util.Misc;

import java.util.HashMap;
import java.util.Map;

public class KH_ruleTokenReplacementPlugin implements RuleTokenReplacementGeneratorPlugin
{
    @Override
    public Map<String, String> getTokenReplacements(String ruleId, Object entity, Map<String, MemoryAPI> memoryMap)
    {
        final Map<String, String> map = new HashMap<>();

        PersonAPI person = null;
        if (entity instanceof SectorEntityToken)
        {
            final SectorEntityToken token = (SectorEntityToken) entity;
            person = token.getActivePerson();
        }

        if (person == null)
        {
            if (entity instanceof CampaignFleetAPI)
            {
                person = ((CampaignFleetAPI) entity).getCommander();
            }
            else if (entity instanceof PersonAPI)
            {
                person = (PersonAPI) entity; // can't actually happen as entity is always a SectorEntityToken
            }
        }

        PersonAPI playerPerson = Global.getSector().getPlayerPerson();
        if (playerPerson != null)
        {
            if (playerPerson.isMale())
            {
                map.put("$playerManOrWoman", "man");
                map.put("$PlayerManOrWoman", "Man");
                map.put("$playerGuyOrGirl", "guy");
                map.put("$PlayerGuyOrGirl", "Guy");
            }
            else
            {
                map.put("$playerManOrWoman", "woman");
                map.put("$PlayerManOrWoman", "Woman");
                map.put("$playerGuyOrGirl", "girl");
                map.put("$PlayerGuyOrGirl", "Girl");
            }
        }

        if (person != null)
        {
            if (person.isMale())
            {
                map.put("$manOrWoman", "man");
                map.put("$ManOrWoman", "Man");
                map.put("$guyOrGirl", "guy");
                map.put("$GuyOrGirl", "Guy");
            }
            else
            {
                map.put("$manOrWoman", "woman");
                map.put("$ManOrWoman", "Woman");
                map.put("$guyOrGirl", "girl");
                map.put("$GuyOrGirl", "Girl");
            }
        }

        if (playerPerson != null)
        {
            final FactionAPI commissionFaction = Misc.getCommissionFaction();
            if (commissionFaction != null)
            {
                String factionName = commissionFaction.getEntityNamePrefix();
                if (factionName == null || factionName.isEmpty())
                {
                    factionName = commissionFaction.getDisplayName();
                }

                map.put("$commissionFactionIsOrAre", commissionFaction.getDisplayNameIsOrAre());

                map.put("$commissionFaction", factionName);
                map.put("$CommissionFaction", Misc.ucFirst(factionName));
                map.put("$theCommissionFaction", commissionFaction.getDisplayNameWithArticle());
                map.put("$TheCommissionFaction", Misc.ucFirst(commissionFaction.getDisplayNameWithArticle()));

                map.put("$commissionFactionLong", commissionFaction.getDisplayNameLong());
                map.put("$CommissionFactionLong", Misc.ucFirst(commissionFaction.getDisplayNameLong()));
                map.put("$theCommissionFactionLong", commissionFaction.getDisplayNameLongWithArticle());
                map.put("$TheCommissionFactionLong", Misc.ucFirst(commissionFaction.getDisplayNameLongWithArticle()));
            }
        }

        return map;
    }
}
