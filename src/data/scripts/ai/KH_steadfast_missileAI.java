//by Tartiflette, Anti-missile missile AI: precise and able to randomly choose a target between nearby enemy missiles.
//feel free to use it, credit is appreciated but not mandatory
//V2 done
package data.scripts.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.GuidedMissileAI;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipCommand;
import com.fs.starfarer.api.combat.WeaponAPI;
import data.scripts.weapons.KH_steadfast_target;
import java.awt.Color;
import org.lazywizard.lazylib.FastTrig;
//import org.lazywizard.lazylib.FastTrig;
import org.lwjgl.util.vector.Vector2f;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;

public class KH_steadfast_missileAI implements MissileAIPlugin, GuidedMissileAI {

    private EveryFrameWeaponEffectPlugin script;
    private CombatEngineAPI engine;
    private final MissileAPI missile;
    private CombatEntityAPI target;
    private final float DAMPING = 0.2f;
    private boolean FIRE=false;

    public KH_steadfast_missileAI(MissileAPI missile, ShipAPI launchingShip) {
        this.missile = missile;  ShipAPI source = missile.getSource();
        if(source.getParentStation()!=null){
            source=source.getParentStation();
        }
        WeaponAPI laser=null;
        for(WeaponAPI w: source.getAllWeapons()){
            if(w.getSpec().getWeaponId().equals("kh_steadfast_beam")){
                laser=w;
                break;
            }
        }
        if(laser!=null){
            script = laser.getEffectPlugin();
        }      
    }

    @Override
    public void advance(float amount) {
        
        if (engine != Global.getCombatEngine()) {
            this.engine = Global.getCombatEngine();
        }
        
        if (Global.getCombatEngine().isPaused() || missile.isFading() || missile.isFizzling()){
            return;
        }
        
//        missile.giveCommand(ShipCommand.ACCELERATE);
            
        if(!missile.getSource().isAlive())return;
        
        Vector2f pointer = getPointer();
        target=getPointerTarget();
        
        if (pointer == null)return;
                


        /////////
        //TARGET VELOCITY AIMING
        
        ///debug
//        engine.addHitParticle(pointer, new Vector2f(), 5, 1, 0.1f, Color.BLUE);
        ///
        
        
        if(target!=null){
            
            ///debug
//            engine.addHitParticle(target.getLocation(), new Vector2f(), 10, 1, 0.1f, Color.RED);
            
//            Vector2f loc = missile.getLocation();
//            Float speed = missile.getMaxSpeed();
//            Vector2f Tloc = pointer;
//            Vector2f Tvel = target.getVelocity();
            
            
            Vector2f newPoint = AIUtils.getBestInterceptPoint(
                    missile.getLocation(),
                    missile.getMaxSpeed()*0.75f,
                    pointer,
                    target.getVelocity()
            );
            
            if(newPoint!=null){
                pointer=newPoint;
            }   
        }
        
        ///debug
//        engine.addHitParticle(pointer, new Vector2f(), 5, 1, 0.1f, Color.GREEN);
        ///
        
        //MISSILE VELOCITY BASED AIMING
        float correctAngle;
        if(FIRE){
        //best velocity vector angle for interception
        correctAngle = VectorUtils.getAngle(
                        missile.getLocation(),
                        pointer
                );
        
        //velocity angle correction
        float offCourseAngle = MathUtils.getShortestRotation(
                VectorUtils.getFacing(missile.getVelocity()),
                correctAngle
                );
        
        float correction = MathUtils.getShortestRotation(                
                correctAngle,
                VectorUtils.getFacing(missile.getVelocity())+180
                ) 
                * 0.5f * //oversteer
                (float)(FastTrig.sin(MathUtils.FPI/180*(Math.min(Math.abs(offCourseAngle),90)))); //damping when the correction isn't important
        
        //modified optimal facing to correct the velocity vector angle as soon as possible
        correctAngle+= correction;
        } else {
        /////////
        
        correctAngle = VectorUtils.getAngle(
                        missile.getLocation(),
                        pointer
                );
        }
        
        //turn the missile
        float aimAngle = MathUtils.getShortestRotation(missile.getFacing(), correctAngle);
        if (aimAngle < 0) {
            missile.giveCommand(ShipCommand.TURN_RIGHT);
        } else {
            missile.giveCommand(ShipCommand.TURN_LEFT);
        }
        
        if(FIRE || Math.abs(aimAngle)<10){
            missile.giveCommand(ShipCommand.ACCELERATE);
            if(!FIRE){
                FIRE=true;
                Global.getSoundPlayer().playSound("atropos_fire", 1, 0.75f, missile.getLocation(), missile.getVelocity());
                for(int i=0; i<10; i++){
                    engine.addSmokeParticle(
                            missile.getLocation(), 
                            MathUtils.getPoint(new Vector2f(), 10*i, missile.getFacing()+180), 
                            5+2*i,
                            0.5f-0.04f*i,
                            0.25f+0.05f*i,
                            new Color(0.5f+0.02f*i,0.3f+0.04f*i,0.3f+0.04f*i));
                }
            }
        }
        
        // Damp angular velocity if we're getting close to the target angle
        if (Math.abs(aimAngle) < Math.abs(missile.getAngularVelocity()) * DAMPING) {
            missile.setAngularVelocity(aimAngle / DAMPING);
        }
    }
    
    private Vector2f getPointer(){        
        if(script!=null){
            return ((KH_steadfast_target)script).getTargetPoint();
        }
        return null;            
    }
    
    private CombatEntityAPI getPointerTarget(){        
        if(script!=null){
            return ((KH_steadfast_target)script).getTarget();
        }
        return null;            
    }
    
    @Override
    public CombatEntityAPI getTarget() {
        return target;
    }

    @Override
    public void setTarget(CombatEntityAPI target) {
        this.target = target;
    }
        public void init(CombatEngineAPI engine) {
    }
}
