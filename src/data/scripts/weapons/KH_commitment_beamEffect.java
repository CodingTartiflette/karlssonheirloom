package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.BeamAPI;
import com.fs.starfarer.api.combat.BeamEffectPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import java.awt.Color;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lwjgl.util.vector.Vector2f;

public class KH_commitment_beamEffect implements BeamEffectPlugin {
    
    private float pitch=1;
    private int hit=1;
    private boolean runOnce=false;
    private ShipAPI ship, target;

    @Override
    public void advance(float amount, CombatEngineAPI engine, BeamAPI beam) {

        if (engine.isPaused()) {
            return;
        }
        
        if(!runOnce){
            runOnce=true;
            ship=beam.getSource();
            target=ship.getShipTarget();
        }
        
        if(target!=null){
            //system's aiming effect
            float facing = ship.getFacing();
            facing=MathUtils.getShortestRotation(
                    facing,
                    VectorUtils.getAngle(ship.getLocation(), target.getLocation())
            );            
            ship.setAngularVelocity(Math.min(90, Math.max(-90, facing*5)));

            Vector2f trail = MathUtils.getPointOnCircumference(new Vector2f(), 200, 180+ship.getFacing());

            ship.addAfterimage(
                    new Color (100,150,50,32),
                    -2+4*(float)Math.random(),
                    -2+4*(float)Math.random(),
                    trail.x,
                    trail.y,
                    0f,
                    0.0f,
                    0.1f,
                    0.25f,
                    true,
                    true,
                    true
                );
        }
        
        //visual effect        
        if (beam.getBrightness() > 0.1f) {
            
            //on Hit, create impact effect
            if (beam.didDamageThisFrame()) {
                engine.addHitParticle(
                        beam.getTo(),
                        beam.getDamageTarget().getVelocity(),
                        25+50*(float)Math.random(),
                        1,
                        0.1f+(float)Math.random()/4,
                            new Color(255, (int)(105+150*Math.random()), 75, 255)
                );
            }
            
            if(beam.getDamageTarget()!=null){
                
                //particles
                float angle=beam.getWeapon().getCurrAngle()+90;
                
                for(int i=0; i<(30*amount); i++){
                    engine.addHitParticle(
                            beam.getTo(),
                            MathUtils.getRandomPointInCone(
                                    new Vector2f(), 
                                    700, 
                                    angle-15, 
                                    angle+15
                            ),
                            2+5*(float)Math.random(),
                            1,
                            0.1f+(float)Math.random()/2,
                            new Color(255, (int)(105+150*Math.random()), 75, 255)
                    );
                }
                
                hit=-1;
            } else{
                hit=1;
            }
            
            //sound pitch (lower when hitting something)
            pitch = Math.min(1, Math.max(0.85f, pitch+hit*amount));
            
            Global.getSoundPlayer().playLoop("KH_aegisBreaker_loop", ship, pitch, 1, ship.getLocation(), ship.getVelocity());
            
            if(hit<0){
                Global.getSoundPlayer().playLoop("KH_aegisBreaker_hit", ship, pitch, 1, ship.getLocation(), ship.getVelocity());
            }
        }     
    }
}