package data.scripts.weapons;

import com.fs.starfarer.api.combat.BeamAPI;
import com.fs.starfarer.api.combat.BeamEffectPlugin;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import java.awt.Color;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

public class KH_steadfast_beamEffect implements BeamEffectPlugin {
    
    private boolean runOnce=false;
    private EveryFrameWeaponEffectPlugin script=null;

    @Override
    public void advance(float amount, CombatEngineAPI engine, BeamAPI beam) {

        if (engine.isPaused()) {
            return;
        }
        
        if(!runOnce){
            runOnce=true;
            script=beam.getWeapon().getEffectPlugin();
        }
        
        if(beam.getBrightness()>0){
            beam.setFringeColor(new Color(beam.getFringeColor().getRed(),beam.getFringeColor().getGreen(),beam.getFringeColor().getBlue(), MathUtils.getRandomNumberInRange(32, 128)));
        }
        
        if(script!=null && beam.getTo()!=null){
            ((KH_steadfast_target)script).setTargetPoint(new Vector2f(beam.getTo()));
//            if(beam.getDamageTarget()!=null){
                ((KH_steadfast_target)script).setTarget(beam.getDamageTarget());
//            }
        }        
        if(beam.didDamageThisFrame()){
            engine.addSmoothParticle(
                    beam.getTo(),
                    beam.getDamageTarget().getVelocity(), 
                    5+(float)Math.random()*10,
                    1,
                    0.05f+(float)Math.random()*0.1f,
                    Color.red
            );
        }
    }
}