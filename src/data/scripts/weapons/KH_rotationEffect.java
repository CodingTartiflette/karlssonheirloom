package data.scripts.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import org.lazywizard.lazylib.MathUtils;

public class KH_rotationEffect implements EveryFrameWeaponEffectPlugin {
    
    private boolean runOnce=false;
    private ShipAPI ship;
    private float offset=0, boost=1, speed=0;
    private ShipSystemAPI system;
    
    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        
        if(!runOnce){
            runOnce=true;
            offset=(float)Math.random()*10;
            ship=weapon.getShip();
            //preparation to use with different ships
            if(ship.isFighter()){
                speed=90;
            } else {
                speed=45;
            }         
            if(ship.getSystem()!=null){
                system=ship.getSystem();
            }
        }
        
        if(engine.isPaused() || !ship.isAlive()) return;
        
        if(system!=null && system.isActive()){
            boost=1+15*system.getEffectLevel();
        } else {
            boost=1;
        }        
        
        weapon.setCurrAngle(MathUtils.clampAngle(ship.getFullTimeDeployed()*(offset+speed*boost)));
    }
}