package data.scripts.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.ShieldAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import java.awt.Color;
import org.lazywizard.lazylib.FastTrig;
import org.lazywizard.lazylib.MathUtils;

public class KH_shieldFicker implements EveryFrameWeaponEffectPlugin {

    
    private ShieldAPI shield;
    private float radius, arc, current, flicker=0;
    private boolean runOnce = false, activate = true, noShield=false;
    private String inner, outer;
    private final String INNERSMALL = "graphics/KH/fx/KH_shields128.png";
    private final String INNERLARGE = "graphics/KH/fx/KH_shields256.png";
    private final String OUTERSMALL = "graphics/fx/shields128ringc.png";
    private final String OUTERLARGE = "graphics/fx/shields256ringc.png";
    private float R=0.4f, G=1f, B=0.2f;
    
    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        
        if(noShield || engine.isPaused())return;
        
        //data collection
        if (!runOnce) {
            runOnce = true;
            
            if(weapon.getShip().getOwner()==-1){
                noShield=true;
                return;
            }
            
            if(weapon.getShip().getShield()==null){
                noShield=true;
                return;
            }
            shield = weapon.getShip().getShield();
            radius = shield.getRadius();
            arc = shield.getArc();
            current = radius*0.9f;
            if (radius<384){
                inner = INNERSMALL;
                outer = OUTERSMALL;
            } else {                
                inner = INNERLARGE;
                outer = OUTERLARGE;
            }
//            R=shield.getInnerColor().getRed()/255;
//            G=shield.getInnerColor().getGreen()/255;
//            B=shield.getInnerColor().getBlue()/255;
        }
        
        if(shield.isOn()){
            if (activate){
                activate=false;
                shield.setActiveArc(arc);
                current = radius*0.95f;                
                flicker=0;
            } else {
                if(current<radius){
                    current = Math.min(current+amount*600, current + 5*amount*(radius-current));
                    if (radius-current < 0.1f){
                        current = radius;
                    }
                }
            }
            if(flicker<1){                
                flicker = Math.min(1, flicker+2*amount);
            }
        } else if(!activate){
            activate=true;
        }
        
        shield.setRadius(current, inner, outer);
        if(flicker<=1){
            
            float ALPHA = Math.min(
                    1,
                    Math.max(
                            flicker,
                            2*flicker + (float)FastTrig.sin(16*MathUtils.FPI*flicker)
                    ));        
            shield.setInnerColor(new Color(R,G,B,ALPHA*0.15f));   
            shield.setRingColor(new Color(ALPHA,ALPHA,ALPHA,0.75f));  
            
            if(flicker==1){
                //bypass the color change once done with it
                flicker=2;
                shield.setInnerColor(new Color(R,G,B,0.15f));   
                shield.setRingColor(new Color(1,1,1,0.75f));  
            }
        }
    }
}
