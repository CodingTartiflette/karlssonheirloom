package data.scripts.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.loading.WeaponSlotAPI;
import java.util.HashMap;
import java.util.Map;

public class KH_ringEffect implements EveryFrameWeaponEffectPlugin {
    
    private boolean runOnce=false;
    private float offset=0;
    private ShipAPI host,ship;
    private WeaponSlotAPI slot;
    private boolean retreatFailed=false;
    
    private final Map<String,Float> OFFSETS = new HashMap<>();
    {
        OFFSETS.put("MODULE0", 0f);
        OFFSETS.put("MODULE1", 0f);
        OFFSETS.put("MODULE2", 90f);
        OFFSETS.put("MODULE3", 90f);
        OFFSETS.put("MODULE4", 180f);
        OFFSETS.put("MODULE5", 180f);
        OFFSETS.put("MODULE6", 270f);
        OFFSETS.put("MODULE7", 270f);
        OFFSETS.put("MODULE8", 120f);
        OFFSETS.put("MODULE9", -120f);
    }
    
    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        
        if (engine.isPaused()) return;
        
        //SETUP        
        if(!runOnce){
            runOnce=true;            
            ship=weapon.getShip();
            host=ship.getParentStation();
            
            if(host!=null){
                offset=OFFSETS.get(ship.getStationSlot().getId());
		slot = ship.getStationSlot();
		ship.ensureClonedStationSlotSpec();
            }
            
            //weapon range fix
            
            if(ship.getParentStation()!=null){
                float rangeMult= ship.getParentStation().getMutableStats().getBallisticWeaponRangeBonus().getBonusMult();
                float thresholdMult= ship.getParentStation().getMutableStats().getWeaponRangeMultPastThreshold().computeMultMod();
                if(rangeMult<1){

                    ship.getMutableStats().getBallisticWeaponRangeBonus().modifyMult(
                            "OCI_inherited", 
                            (1+rangeMult)/2
                    );
                    ship.getMutableStats().getEnergyWeaponRangeBonus().modifyMult(
                            "OCI_inherited", 
                            (1+rangeMult)/2
                    );
                    ship.getMutableStats().getBeamWeaponRangeBonus().modifyMult(
                            "OCI_inherited", 
                            (1+rangeMult)/2
                    );
                }
                if(thresholdMult<1){
                    ship.getMutableStats().getWeaponRangeThreshold().modifyMult(
                            "OCI_inherited",
                            ship.getParentStation().getMutableStats().getWeaponRangeThreshold().computeMultMod()
                    );
                    ship.getMutableStats().getWeaponRangeMultPastThreshold().modifyMult(
                            "OCI_inherited",
                            (1+thresholdMult)/2
                    );
                }
            }
            
            return;
        }
        
        //retreat fix
        
        if(retreatFailed){
            engine.removeEntity(ship);
            return;
        }
        
        if(!engine.isEntityInPlay(host)&&ship.isAlive()){
            engine.getFleetManager(ship.getOriginalOwner()).getTaskManager(false).orderRetreat(engine.getFleetManager(ship.getOriginalOwner()).getDeployedFleetMember(ship), false, true);
            retreatFailed=true;
            return;
        }
        
        if(host==null || !host.isAlive() || host.isHulk()){
            return;
        }
        
        ship.ensureClonedStationSlotSpec();
	slot = ship.getStationSlot();	
        
	if (slot == null) return;
        
        float angle = offset + +engine.getTotalElapsedTime(false)*10;
        angle -=host.getFacing();
        
        slot.setAngle(angle);
        
//        weapon.getShip().setFacing(offset + engine.getTotalElapsedTime(false)*10 - host.getFacing());        
    }
}