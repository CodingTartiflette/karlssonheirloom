package data.scripts.weapons;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CollisionClass;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipAPI.HullSize;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.scripts.util.MagicRender;
import java.awt.Color;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

public class KH_fleetTelEffect implements EveryFrameWeaponEffectPlugin {
    
    private boolean runOnce=false, pulse=false;
    private ShipAPI ship;
    private ShipSystemAPI system;
    private final float RANGE=2000f;
    
    private Map<ShipAPI, Vector2f> TELEPORT = new HashMap<>();
    private HashMap<Integer,ShipAPI> RECALLED = new HashMap<>();
    
    private final Map<HullSize, Integer> WEIGHT = new HashMap<>();
    {
        WEIGHT.put(HullSize.DEFAULT, 1);
        WEIGHT.put(HullSize.FRIGATE, 1);
        WEIGHT.put(HullSize.DESTROYER, 10);
        WEIGHT.put(HullSize.CRUISER, 100);
        WEIGHT.put(HullSize.CAPITAL_SHIP, 1000);
        WEIGHT.put(HullSize.FIGHTER, 0);
    }
        
    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
                
        if(!runOnce){
            runOnce=true;
            ship=weapon.getShip();
            system = ship.getSystem();
            TELEPORT.clear();
            return;
        }        
                
        if(engine.isPaused() || (!ship.isAlive() && TELEPORT.isEmpty())) return;
        
        if(!TELEPORT.isEmpty()){
            teleport(engine, amount);
        }
                
        if(system.isActive()){
            
            /*
            CHARGEUP:
            Push away enemies along a growing radius
            CHARGEDOWN
            Teleport back the 8 closest allies starting from the farthest
            */
            
            //CHARGEUP            
            if(system.getEffectLevel()==1 || pulse){
                
                //visual effect
                if(!pulse){
                    pulse=true;    
                    float growth=1.75f*RANGE/system.getChargeUpDur();
                    MagicRender.battlespace(
                            Global.getSettings().getSprite("graphics/fx/wormhole_ring_bright3.png"),
                            ship.getLocation(),
                            ship.getVelocity(),
                            new Vector2f(100,100),
                            new Vector2f(growth,growth),
                            0,
                            0,
                            new Color(200,50,100,255),
                            true,
                            0.1f,
                            system.getChargeUpDur()/2,
                            system.getChargeUpDur()/2
                    );
                    
                    RECALLED = getRecalled();
                }
                
                float charge=1-system.getEffectLevel();
                
                //move enemies away
                for(ShipAPI s : AIUtils.getNearbyEnemies(ship, (RANGE)*charge)){
                    if(s.isStation()||s.isStationModule())continue;
                    Vector2f move = MathUtils.getPointOnCircumference(
                            new Vector2f(),
                            RANGE*0.75f*charge,
                            VectorUtils.getAngle(ship.getLocation(), s.getLocation())
                            );                
                    
                    move.scale(2*amount*(1-charge));                    
                    Vector2f.add(s.getLocation(), move, s.getLocation());
                }
                
                //move selected allies back
                for (Iterator <Integer> iter = RECALLED.keySet().iterator(); iter.hasNext();){
                    Integer s = iter.next();
                    
                    if(MathUtils.isWithinRange(ship, RECALLED.get(s), RANGE*charge)){
                        //teleport ship in range
                        RECALLED.get(s).setCollisionClass(CollisionClass.FIGHTER);
                        TELEPORT.put(RECALLED.get(s), MathUtils.getPoint(ship.getLocation(), ship.getCollisionRadius()+RECALLED.get(s).getCollisionRadius()+100, s));
                        iter.remove();
                    }
                }
            }
        } else {
            //reset
            pulse=false;
            RECALLED.clear();
            for(ShipAPI s : TELEPORT.keySet()){
                if(s.getCollisionClass()!=CollisionClass.SHIP){
                    s.setCollisionClass(CollisionClass.SHIP);
                }
            }
            TELEPORT.clear();
        }
    }
    
    private void teleport(CombatEngineAPI engine, float amount){
        for (Iterator <ShipAPI> iter = TELEPORT.keySet().iterator(); iter.hasNext();){
            ShipAPI C = iter.next();
            if(!engine.isEntityInPlay(C) || MathUtils.isWithinRange(C.getLocation(), TELEPORT.get(C), 30)){
                if(engine.isEntityInPlay(C)){
                    C.setCollisionClass(CollisionClass.SHIP);
                }
                iter.remove();
            } else {
                Vector2f targetPos = new Vector2f(TELEPORT.get(C));
                Vector2f.sub(targetPos, C.getLocation(), targetPos);
                targetPos.scale(10*amount);
                
                C.addAfterimage(
                        new Color (255,255,255,50),
                        0,      
                        0,      
                        -10*targetPos.x,
                        -10*targetPos.y,
                        1f,
                        0.1f,
                        0.1f,
                        0.1f,
                        false,
                        true,
                        false
                );
                
                Vector2f LOC = C.getLocation();
                Vector2f.add(LOC, targetPos, LOC);                
                C.getVelocity().scale(0.9f);
            }
        }
    }
    
    private HashMap<Integer,ShipAPI> getRecalled(){
        //Integer is the slot around the recalling ship
        //ShipAPI is the ally recalled to that slot
                
        HashMap<Integer,ShipAPI> recalled= new HashMap<>();
        {
            recalled.put(0, null);
            recalled.put(45, null);
            recalled.put(90, null);
            recalled.put(135, null);
            recalled.put(180, null);
            recalled.put(225, null);
            recalled.put(270, null);
            recalled.put(315, null);
        }
        
        WeightedRandomPicker<ShipAPI> targetPicker = new WeightedRandomPicker<>();        
        List<ShipAPI> candidates = AIUtils.getNearbyAllies(ship, RANGE);
        
        if(candidates!=null){
            for(ShipAPI s: candidates){

                if(!s.isAlive())continue;
                if(s.getCollisionClass()!= CollisionClass.SHIP)continue;
                if(s.getParentStation()!=null)continue;
                if(s.isStation()||s.isDrone()||s.isFighter())continue;

                if(s.getHullSize()!=null){
                    float weight = WEIGHT.get(s.getHullSize());
                    //ship in danger are priority candidates:
                    if(s.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.IN_CRITICAL_DPS_DANGER)){
                        weight*=10;
                    }
                    if(MathUtils.isWithinRange(ship, s, 500)){
                        weight*=10;
                    }
                    targetPicker.add(s, weight);
                }
            }
        }
        
        for(int i=0; i<8; i++){
            if(targetPicker.isEmpty())break;
            
            ShipAPI picked = targetPicker.pickAndRemove();
            
            //find a slot to recall them to
            
            float angle = VectorUtils.getAngle(ship.getLocation(), picked.getLocation());
            int slotAngle = (int)(Math.round(angle/45)*45);
            
            for(int j=1; j<=8; j++){
                if(recalled.get(slotAngle)==null){
                    recalled.put(slotAngle, picked);
                    break;
                } else {
                    //next slot
                    int rotation = Math.round(MathUtils.getShortestRotation(angle, slotAngle));
                    if(rotation>0){rotation=1;}
                    else{rotation=-1;}
                    
                    slotAngle = slotAngle - rotation * j * 45;
                }
            }
        }
        
        //cleanup the empty slots
        for(Iterator<Integer> iter=recalled.keySet().iterator(); iter.hasNext(); ){
            Integer entry = iter.next();
            if(recalled.get(entry)==null){
                iter.remove();
            }
        }
        
        return recalled;
    }
}