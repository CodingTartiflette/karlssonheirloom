package data.scripts.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.CombatEntityAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.WeaponAPI;
import org.lwjgl.util.vector.Vector2f;

public class KH_steadfast_target implements EveryFrameWeaponEffectPlugin {
        
    public Vector2f TARGET_POINT;
    public CombatEntityAPI TARGET;
    private boolean updated;
    
    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        
        if (engine.isPaused()) {
            return;
        }
        
        if(!updated){
            TARGET_POINT=null;
            TARGET=null;
        } else {
            updated=false;
        }
    }
    
    public Vector2f getTargetPoint() {
        return TARGET_POINT;
    }
    
    public void setTargetPoint(Vector2f point) {
        TARGET_POINT = point;
        updated=true;
    }
    
    public CombatEntityAPI getTarget() {
        return TARGET;
    }
    
    public void setTarget(CombatEntityAPI target) {
        TARGET = target;
    }
}