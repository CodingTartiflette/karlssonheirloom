package data.scripts.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import java.awt.Color;

public class KH_diligenceEffect implements EveryFrameWeaponEffectPlugin {
    
    private boolean runOnce=false;
    private ShipAPI ship;
    private ShipSystemAPI system;
    private float offset=0;    
    private float size=0, timer=0;
    
    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        
        if(!runOnce){
            runOnce=true;
            offset=(float)Math.random()*10;
            ship=weapon.getShip();
            //preparation to use with different ships
            if(ship.isFighter()){
                size=0;
                system = ship.getSystem();
            } else {
                size=1;
            }
        }
        
        if(engine.isPaused() || !weapon.getShip().isAlive()) return;
                
        weapon.setCurrAngle(engine.getTotalElapsedTime(false)*(135+offset)); 
        
        if(size==0 && system.isActive()){                       
            //fighter trail
            
            /*
            public void addAfterimage(
                Color color,
                float locX,
                float locY,
                float velX,
                float velY,
                float maxJitter,
                float in,
                float dur,
                float out,
                boolean additive,
                boolean combineWithSpriteColor,
                boolean aboveShip) 
            */
            
            timer+=amount;
            if(timer>0.05f){
                timer=0;
                
                ship.addAfterimage(
                    new Color (255,255,255,50),
                    0,      
                    0,
                    -0.5f*ship.getVelocity().x,
                    -0.5f*ship.getVelocity().y,
                    0f,
                    0.0f,
                    0.1f,
                    0.25f,
                    true,
                    true,
                    false
                );
            }
        }
    }
}