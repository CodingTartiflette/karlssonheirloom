package data.scripts.weapons;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import java.awt.Color;
import org.lazywizard.lazylib.MathUtils;
import org.lwjgl.util.vector.Vector2f;

public class KH_steadfast_systemSlotEffect implements EveryFrameWeaponEffectPlugin {
    
    private boolean runOnce=false, launched=false;
    private ShipAPI ship;
    private ShipSystemAPI system;
    private IntervalUtil timer=new IntervalUtil(0.05f,0.15f); 
    
    @Override
    public void advance(float amount, CombatEngineAPI engine, WeaponAPI weapon) {
        
        if(!runOnce){
            runOnce=true;
            ship=weapon.getShip();
            
            if(ship.getParentStation()==null){
                system=ship.getSystem();
            } else {
                system=ship.getParentStation().getSystem();
            }
        }
        
        if(engine.isPaused() || !ship.isAlive() || system==null) return;
        
        timer.advance(amount);
        if(timer.intervalElapsed()){
            if(system.isActive()){
                if(!launched){
                    engine.spawnProjectile(
                            ship,
                            weapon,
                            "kh_steadfast_missile",
                            weapon.getLocation(),
                            weapon.getCurrAngle(),
                            ship.getVelocity()
                    );
                    
                    for(int i=0; i<7; i++){
                        Vector2f vel = new Vector2f(ship.getVelocity());
                        Vector2f.add(
                                vel, 
                                MathUtils.getRandomPointInCone(
                                        new Vector2f(),
                                        75,
                                        weapon.getCurrAngle()-15,
                                        weapon.getCurrAngle()+15
                                ),
                                vel
                        );
                        float grey = 0.1f+0.25f*(float)Math.random();
                        engine.addSmokeParticle(
                                weapon.getLocation(), 
                                vel, 
                                5+15*(float)Math.random(), 
                                0.2f+0.3f*(float)Math.random(), 
                                0.25f+0.5f*(float)Math.random(), 
                                new Color(grey,grey,grey)
                        );
                    }                    
                    launched=true;
                }
            }
        }
        if(launched && !system.isActive()){
            launched=false;
        }
    }
}