package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import org.lazywizard.lazylib.MathUtils;

public class KH_rushStats extends BaseShipSystemScript {
    
    private final float RANGE=750, SPEED=100, DAMAGE_REDUCTION=0.9f;
    
    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        
        if (state == ShipSystemStatsScript.State.OUT) {
            stats.getMaxSpeed().unmodify(id);
        } else {
            stats.getMaxSpeed().modifyFlat(id, SPEED * effectLevel);
            stats.getAcceleration().modifyFlat(id, SPEED * 2 * effectLevel);
        }
        
        stats.getArmorDamageTakenMult().modifyMult(id, (1-effectLevel)*DAMAGE_REDUCTION);        
        stats.getHullDamageTakenMult().modifyMult(id, (1-effectLevel)*DAMAGE_REDUCTION);        
    }


    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {        
        stats.getMaxSpeed().unmodify(id);
        stats.getAcceleration().unmodify(id);
        stats.getArmorDamageTakenMult().unmodify(id);
        stats.getHullDamageTakenMult().unmodify(id);
    }
    
    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
            return new StatusData("Reactionless drive field overdrive: "+ (int)(100*effectLevel) + "%", false);
        }
        return null;
    }

    @Override
    public String getInfoText(ShipSystemAPI system, ShipAPI ship) {
        if (system.isOutOfAmmo()) return null;
        if (system.getState() != ShipSystemAPI.SystemState.IDLE) return null;

        ShipAPI target = findTarget(ship);
        if (target != null && target != ship) {
            return "READY";
        }
        
        if ((target == null || target == ship) && ship.getShipTarget() != null) {
            return "OUT OF RANGE";
        }
        return "NO TARGET";
    }
    
    protected ShipAPI findTarget(ShipAPI ship) {
        ShipAPI target = ship.getShipTarget();
        if(
                target!=null 
                && 
                (!target.isDrone()||!target.isFighter()) 
                && 
                MathUtils.isWithinRange(ship, target, RANGE) 
                && 
                target.getOwner()!=ship.getOwner()
                ){
            return target;
        } else {
            return null;
        }
    }

    @Override
    public boolean isUsable(ShipSystemAPI system, ShipAPI ship) {
        ShipAPI target = findTarget(ship);
        return target != null && target != ship;
    }


}
