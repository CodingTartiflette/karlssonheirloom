package data.shipsystems.scripts;

import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;

public class KH_zipStats extends BaseShipSystemScript {

    @Override
    public void apply(MutableShipStatsAPI stats, String id, State state, float effectLevel) {
        
        stats.getMaxSpeed().modifyFlat(id, 400f*effectLevel);
        stats.getAcceleration().modifyFlat(id, 800f);
        stats.getDeceleration().modifyFlat(id, 800f);
        stats.getTurnAcceleration().modifyFlat(id, 720f);
        stats.getMaxTurnRate().modifyFlat(id, 360f*effectLevel);
        
    }
    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
        stats.getMaxSpeed().unmodify(id);
        stats.getMaxTurnRate().unmodify(id);
        stats.getTurnAcceleration().unmodify(id);
        stats.getAcceleration().unmodify(id);
        stats.getDeceleration().unmodify(id);
    }

    @Override
    public StatusData getStatusData(int index, State state, float effectLevel) {
        if (index == 0) {
                return new StatusData("+300 top speed", false);
        }
        return null;
    }
}
