package data.shipsystems.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.DamageType;
import com.fs.starfarer.api.combat.MutableShipStatsAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponSize;
import com.fs.starfarer.api.impl.combat.BaseShipSystemScript;
import com.fs.starfarer.api.plugins.ShipSystemStatsScript;
import java.awt.Color;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.entities.SimpleEntity;

public class KH_rationale_lockStats extends BaseShipSystemScript {

    public static final float RANGE = 750f;
    public static final Color COLOR = new Color(100,165,255,75);	

    @Override
    public void apply(MutableShipStatsAPI stats, final String id, ShipSystemStatsScript.State state, float effectLevel) {
        
        if(effectLevel!=1) return;
        
        ShipAPI ship;
        if (stats.getEntity() instanceof ShipAPI) {
                ship = (ShipAPI) stats.getEntity();
        } else {
                return;
        }
        
        ShipAPI target = findTarget(ship);
        if(target==null) return;
        
        if(target.getUsableWeapons().isEmpty()) return;
        
        float amount = target.getUsableWeapons().size();
        float fraction = 0;
        
        for(WeaponAPI w: target.getUsableWeapons()){     
            if(w.isDisabled()){
                amount--;
                continue;
            }
            
            if(!w.isFiring()){
                w.setRemainingCooldownTo(w.getCooldown());
            }
            
            float chance=0.15f;
            if(w.getSize()==WeaponSize.LARGE){
                chance=0.05f;
            } else if(w.getSize()==WeaponSize.MEDIUM){
                chance=0.1f;
            }
            if (w.hasAIHint(WeaponAPI.AIHints.PD)){
                chance*=3;
            }            
            chance+=fraction/amount;
            
            if(Math.random()<chance){
                w.disable(false);
                zap(ship,w);
                fraction++;
            } else {
                fraction++;
            }            
        }
    }
    
    private void zap (ShipAPI ship, WeaponAPI target){
        /*
        public CombatEntityAPI spawnEmpArc(
            ShipAPI damageSource, 
            Vector2f point,
            CombatEntityAPI pointAnchor,
            CombatEntityAPI empTargetEntity, 
            DamageType damageType, 
            float damAmount, 
            float empDamAmount, 
            float maxRange, 
            String impactSoundId, 
            float thickness, 
            Color fringe,
            Color core
        )
        */
        
        SimpleEntity hit = new SimpleEntity(target.getLocation());
        
        Global.getCombatEngine().spawnEmpArc(
                ship, 
                MathUtils.getRandomPointOnCircumference(ship.getLocation(), 10),
                hit,
                hit,
                DamageType.KINETIC, 
                0,
                0, 
                3000,
                null, 
                3,
                COLOR,
                Color.WHITE
        );
    }

    @Override
    public void unapply(MutableShipStatsAPI stats, String id) {
    }

    protected ShipAPI findTarget(ShipAPI ship) {
        ShipAPI target = ship.getShipTarget();
        if(
                target!=null 
                &&
                (!target.isDrone()||!target.isFighter())
                && 
                MathUtils.isWithinRange(ship, target, RANGE) 
                && 
                target.getOwner()!=ship.getOwner()
                ){
            return target;
        } else {
            return null;
        }
    }

    @Override
    public ShipSystemStatsScript.StatusData getStatusData(int index, ShipSystemStatsScript.State state, float effectLevel) {
        return null;
    }

    @Override
    public String getInfoText(ShipSystemAPI system, ShipAPI ship) {
        if (system.isOutOfAmmo()) return null;
        if (system.getState() != ShipSystemAPI.SystemState.IDLE) return null;

        ShipAPI target = findTarget(ship);
        if (target != null && target != ship) {
            return "READY";
        }
        if (target == null && ship.getShipTarget() != null) {
            return "OUT OF RANGE";
        }
        return "NO TARGET";
    }

    @Override
    public boolean isUsable(ShipSystemAPI system, ShipAPI ship) {
        if (system.isActive()) return true;
        ShipAPI target = findTarget(ship);
        return target != null && target != ship;
    }
}