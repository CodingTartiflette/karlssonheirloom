
package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.EveryFrameWeaponEffectPlugin;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.util.IntervalUtil;
import data.scripts.weapons.KH_steadfast_target;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

public class KH_precisionStrikeAI implements ShipSystemAIScript{
    
    private ShipAPI ship;
    private CombatEngineAPI engine;
    private ShipSystemAPI system;
    private EveryFrameWeaponEffectPlugin script;

    private boolean runOnce=false;
    private WeaponAPI laser=null;
    private IntervalUtil tracker = new IntervalUtil(0.5f, 2f);

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
        this.ship = ship;
        this.engine = engine;
        this.system = system;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        
        if(engine.isPaused()) return;
        
        if(!runOnce){
            runOnce=true;
            for(WeaponAPI w: ship.getAllWeapons()){
                if(w.getSpec().getWeaponId().equals("kh_steadfast_beam")){
                    laser=w;
                    script = laser.getEffectPlugin();
                    break;
                }
            }
        }
        
        if(laser==null)return;
        
        if(!system.isActive()){
            //activation AI
            tracker.advance(amount);
            if(tracker.intervalElapsed()){
                if(!laser.isFiring()) return;
                
                ShipAPI laserTarget=null;
                if(script!=null){
                    if(((KH_steadfast_target)script).getTarget() instanceof ShipAPI){
                        laserTarget = (ShipAPI)((KH_steadfast_target)script).getTarget();
                    }
                }
                if(laserTarget==null) return;
                
                if(!AIUtils.canUseSystemThisFrame(ship)) return; 
                
                //offensive use
                if(
                        laserTarget.getShield()==null
                        ||
                        laserTarget.getFluxTracker().isOverloadedOrVenting()
                        ||
                        laserTarget.getFluxTracker().getFluxLevel()>0.25f+(float)Math.random()/2
                        ){                    
                    ship.useSystem();
                    return;
                }
                
                //defensive use
                if(
                        ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.HAS_INCOMING_DAMAGE)
                        ||
                        ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.IN_CRITICAL_DPS_DANGER)
                        ||
                        ship.getFluxTracker().getFluxLevel()>0.8f
                        )
                    ship.useSystem();
            }
        }
    }
}
