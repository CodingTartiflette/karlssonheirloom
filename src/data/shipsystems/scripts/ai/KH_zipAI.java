package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import java.awt.Color;
import org.apache.log4j.Logger;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

public class KH_zipAI implements ShipSystemAIScript{    
    
    private Logger log = Global.getLogger(KH_zipAI.class);    
    
    private CombatEngineAPI engine;
    private ShipAPI ship;
    private ShipSystemAPI system;
    private IntervalUtil timer = new IntervalUtil(1f,1.5f);
    private final float TICK=1f;

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine){
        this.ship = ship;
        this.system = system;
        this.engine = engine;
    }

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target){
        
        if(engine.isPaused()){
            return;
        }
        
        if(!system.isActive()){
            timer.advance(amount);
            if(timer.intervalElapsed()){
                if(!system.isActive() && AIUtils.canUseSystemThisFrame(ship)){
                    ship.useSystem();
                }
            }
        }
    }
}