package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

public class KH_fleetTelAI implements ShipSystemAIScript{    
    
    private Logger log = Global.getLogger(KH_fleetTelAI.class);    
    
    private CombatEngineAPI engine;
    private ShipAPI ship;
    private ShipSystemAPI system;
    private IntervalUtil timer= new IntervalUtil (1,2);
    private final float RANGE=1500f;
    

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine){
        this.ship = ship;
        this.system = system;
        this.engine = engine;
    }

    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target){
        
        if(engine.isPaused() || ship.getAIFlags()==null){
            return;
        }
        
        if(!system.isActive()){
            //trigger
            timer.advance(amount);
            if(timer.intervalElapsed()){
                
                /*
                TRIGGERS:
                Ship attacked and at high flux or taking damage
                Nearby ally taking damage                
                */
                
                //system can be used  
                if(ship.getFluxTracker().getFluxLevel()<0.9 && AIUtils.canUseSystemThisFrame(ship)){    
                    
                    //Allies present in range
                    List <ShipAPI> allies=new ArrayList<>();
                    
                    for (ShipAPI s : AIUtils.getNearbyAllies(ship, RANGE)){
                        //only keep the relevant ships
                        if(
                                !s.isAlive() ||
                                s.getAIFlags()==null || 
                                s.getParentStation()!=null || 
                                s.isFighter() ||
                                s.isDrone()
                                ){
                            allies.add(s);                 
                        }
                    }

//                        //ignore if too few allies nearby
//                        if(allies.size()<2){
//                            return;
//                        }

                    boolean useSystem=false;

                    //evaluate for ship                        
                    if(
                            ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.IN_CRITICAL_DPS_DANGER)||
                            ship.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.NEEDS_HELP)||
                            (ship.getFluxTracker().getFluxLevel()>0.7 && ship.areSignificantEnemiesInRange())
                        ){
                        useSystem=true;
                    }

                    if(!useSystem){
                        //evaluate for allies
                        for(ShipAPI s : allies){
                            if(
                                    s.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.IN_CRITICAL_DPS_DANGER)||
                                    s.getAIFlags().hasFlag(ShipwideAIFlags.AIFlags.NEEDS_HELP)||
                                    (s.getFluxTracker().isOverloadedOrVenting()&& s.areSignificantEnemiesInRange())
                                    ){
                                useSystem=true;
                                break;
                            }
                        }
                    }

                    if(useSystem){
                        ship.useSystem();
                    }
                }
            }
        }
    }
}