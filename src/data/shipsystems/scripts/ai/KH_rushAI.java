package data.shipsystems.scripts.ai;

import com.fs.starfarer.api.combat.CombatEngineAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import com.fs.starfarer.api.combat.ShipSystemAIScript;
import com.fs.starfarer.api.combat.ShipSystemAPI;
import com.fs.starfarer.api.combat.ShipwideAIFlags;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.combat.AIUtils;
import org.lwjgl.util.vector.Vector2f;

public class KH_rushAI implements ShipSystemAIScript{
    
    private ShipAPI ship;
    private CombatEngineAPI engine;
    private ShipwideAIFlags flags;
    private ShipSystemAPI system;

    private IntervalUtil tracker = new IntervalUtil(0.5f, 1f);

    @Override
    public void init(ShipAPI ship, ShipSystemAPI system, ShipwideAIFlags flags, CombatEngineAPI engine) {
            this.ship = ship;
            this.flags = flags;
            this.engine = engine;
            this.system = system;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public void advance(float amount, Vector2f missileDangerDir, Vector2f collisionDangerDir, ShipAPI target) {
        if(engine.isPaused()) return;
        
        if(!system.isActive()){
            //activation AI
            tracker.advance(amount);
            if(tracker.intervalElapsed()){
//                ShipAPI theTarget = ship.getShipTarget();
//                if(theTarget==null){
//                    ShipAPI newTarget=MagicTargeting.pickTarget(
//                            ship,
//                            MagicTargeting.targetSeeking.FULL_RANDOM,
//                            750,
//                            360,
//                            0,
//                            1,
//                            25,
//                            50,
//                            10,
//                            true
//                    );
//                    
//                    if(newTarget!=null){
//                        theTarget=newTarget;
//                        ship.setShipTarget(newTarget);
//                    } else {
//                        return;
//                    }
//                }
                if(target==null){return;}
                if(ship.getShipTarget()==null){
                    ship.setShipTarget(target);
                    return;
                }
                if(!target.isAlive()) return;
                if(target.isFighter() || target.isDrone()) return;
                if(!MathUtils.isWithinRange(ship, target, 1000))
                if(!AIUtils.canUseSystemThisFrame(ship)) return;
                if(ship.getFluxTracker().getFluxLevel()>0.5f) return;
                
                if(
                        flags.hasFlag(ShipwideAIFlags.AIFlags.MANEUVER_TARGET)
                        ||
                        flags.hasFlag(ShipwideAIFlags.AIFlags.PURSUING)
                        ||
                        flags.hasFlag(ShipwideAIFlags.AIFlags.HARASS_MOVE_IN)
                        ){
                    
                    ship.useSystem();
                }
            }
        }
    }
}
