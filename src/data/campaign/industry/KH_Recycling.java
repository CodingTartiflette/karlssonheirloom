package data.campaign.industry;

import com.fs.starfarer.api.impl.campaign.econ.impl.BaseIndustry;
import com.fs.starfarer.api.impl.campaign.ids.Commodities;

public class KH_Recycling extends BaseIndustry {

    @Override
    public void apply() {
        
        super.apply(true);
        
        int size = market.getSize();
        
        supply(Commodities.FUEL, size);
        supply(Commodities.SUPPLIES, size);
        supply(Commodities.FOOD, size);
        supply(Commodities.ORGANICS, size);
        supply(Commodities.ORGANS, size);
        supply(Commodities.METALS, size);
        supply(Commodities.RARE_METALS, size);
        supply(Commodities.DRUGS, size);
        supply(Commodities.SHIPS, size);
        supply(Commodities.DOMESTIC_GOODS, size);
        supply(Commodities.LUXURY_GOODS, size);

        if (!isFunctional()) {
                supply.clear();
        }
    }
        
    @Override
    public void unapply() {
        super.unapply();
    }

    @Override
    public boolean isAvailableToBuild() {
            return false;
    }

    @Override
    public boolean showWhenUnavailable() {
            return false;
    }
}