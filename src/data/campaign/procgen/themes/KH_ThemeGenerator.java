package data.campaign.procgen.themes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.lwjgl.util.vector.Vector2f;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CustomCampaignEntityAPI;
import com.fs.starfarer.api.campaign.JumpPointAPI;
import com.fs.starfarer.api.campaign.JumpPointAPI.JumpDestination;
import com.fs.starfarer.api.campaign.OrbitAPI;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI.SurveyLevel;
import com.fs.starfarer.api.characters.FullName;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Entities;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Industries;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;
import com.fs.starfarer.api.impl.campaign.intel.deciv.DecivTracker;
import data.campaign.ids.KH_Tags;
import com.fs.starfarer.api.impl.campaign.procgen.Constellation;
import com.fs.starfarer.api.impl.campaign.procgen.StarSystemGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.BaseThemeGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.ThemeGenContext;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.campaign.ids.KH_Entities;
import data.campaign.ids.KH_Factions;
import data.campaign.ids.KH_Industries;
import java.awt.Color;


public class KH_ThemeGenerator extends BaseThemeGenerator {

    public static enum OCISystemType {
        SENTINEL(KH_Tags.THEME_OCI_SMALL, "$ociSentinel"),
        BORDER_PATROL(KH_Tags.THEME_OCI_LARGE, "$ociPatrol"),
        CORE(KH_Tags.THEME_OCI_CORE, "$ociCore"),
        ;

        private String tag;
        private String beaconFlag;
        private OCISystemType(String tag, String beaconFlag) {
                this.tag = tag;
                this.beaconFlag = beaconFlag;
        }
        public String getTag() {
                return tag;
        }
        public String getBeaconFlag() {
                return beaconFlag;
        }
    }
    
    private final int MIN_SYSTEMS_WITH_OCI = 6;
    private final int MAX_SYSTEMS_WITH_OCI = 9;
    private final int SIZE_SMALL = 3;
    private final int SIZE_MEDIUM = 5;
    private final int SIZE_LARGE = 7;
    
    
    @Override
    public String getThemeId() {
        return KH_Themes.OCI;
    }

    @Override
    public void generateForSector(ThemeGenContext context, float allowedUnusedFraction) {

        if(context.majorThemes.containsValue(KH_Themes.OCI) ){     
            if (DEBUG) System.out.println("OCI systems already generated");
            return;
        }
        
        float total = (float) (context.constellations.size() - context.majorThemes.size()) * allowedUnusedFraction;
        if (total <= 0) return;

        int num = (int) StarSystemGenerator.getNormalRandom(MIN_SYSTEMS_WITH_OCI, MAX_SYSTEMS_WITH_OCI);
        if (num > total) num = (int) total;

        //ADD CORE FIRST

        Vector2f center = new Vector2f();
        List<Constellation> constellations = getSortedAvailableConstellations(context, false, center, null);
        WeightedRandomPicker<Constellation> picker = new WeightedRandomPicker<>(StarSystemGenerator.random);

        //only pick from constellations on the 1/3rd outermost part of the sector
        for (int i = 0; i < constellations.size() / 3; i++) {
            if(constellations.get(i).getSystems().size()>2){
                picker.add(constellations.get(i));
            }
        }
        Constellation main = picker.pick();
        
        if (main == null){
            if (DEBUG) System.out.println("Selecting OCI systems"+
                "\n"+
                "No suitable constellation");
            return;
        }

        //pick main system in constellation
        StarSystemAPI mainSystem = main.getSystemWithMostPlanets();
        if (mainSystem == null) {
            if (DEBUG) System.out.println("Selecting OCI systems"+
                "\n"+
                "No suitable system");
            return;
        }

        //found core constellation and system           
        if (DEBUG) System.out.println("\n\n\n");
        if (DEBUG) System.out.println("Selecting OCI systems"+
                "\n"+
                "Picked CORE constellation: "+main.getName());                
        if (DEBUG) System.out.println("Selected main system: "+mainSystem.getName()+"\n");
        context.majorThemes.put(main, KH_Themes.OCI);     
        mainSystem.addTag(KH_Tags.THEME_OCI);

        //Adding secondaries and tertiary systems

        List<StarSystemAPI> MINOR=new ArrayList<>();
        List<StarSystemAPI> MAJOR=new ArrayList<>();

        //Adding presence in main constellation
        for(StarSystemAPI s : main.getSystems()){
            if(s==mainSystem) continue;
            if(s.hasPulsar()) continue;
            if(s.getPlanets().isEmpty()) continue;

            if(s.getPlanets().size()<2 || MAJOR.size()>MINOR.size()){
                //add minor presence
                MINOR.add(s);
                if (DEBUG) System.out.println("Adding minor presence to system: "+s.getName());
            } else {
                //add major presence
                MAJOR.add(s);
                if (DEBUG) System.out.println("Adding major presence to system: "+s.getName());
            }
        }

        //Adding more presence if necessary in nearby constellations
        if(num > MINOR.size()+MAJOR.size()){     
            //searching around the CORE system
            constellations = getSortedAvailableConstellations(context, false, main.getLocation(), null);
            Collections.reverse(constellations);

            //only pick from constellations nearby
            for (int i = 0; i < constellations.size() / 4; i++) {
                if(constellations.get(i)== main) continue;

                Constellation C=constellations.get(i);                        
                if (DEBUG) System.out.println("Checking "+C.getName()+" constelation"+"\n");

                for(StarSystemAPI s : C.getSystems()){
                    if(s.hasPulsar()) continue;
                    if(s.getPlanets().isEmpty()) continue;

                    if(s.getPlanets().size()<4){
                        //add minor presence
                        if (DEBUG) System.out.println("Adding minor presence to system: "+s.getName());
                        context.majorThemes.put(C, KH_Themes.OCI);      
                        s.addTag(KH_Tags.THEME_OCI); 
                        MINOR.add(s);
                    } else {
                        //add major presence
                        if (DEBUG) System.out.println("Adding major presence to system: "+s.getName());
                        context.majorThemes.put(C, KH_Themes.OCI);     
                        s.addTag(KH_Tags.THEME_OCI); 
                        MAJOR.add(s);
                    }
                }
                //stops when enough systems have been populated
                if(num<MINOR.size()+MAJOR.size()){
                    break;
                }
            }
        }
        
        if (DEBUG) System.out.println("\n\n\n");
        if (DEBUG) System.out.println("Generating OCI systems");
                
        //MINOR systems generation
        for (StarSystemAPI system : MINOR) {
            StarSystemData data = computeSystemData(system);
            SectorEntityToken station = addBattlestations(data,KH_Entities.SMALL_STATION);
            populateNonMain(data);

//            KH_StationFleetManager activeFleets = new KH_StationFleetManager(
//                    station,
//                    1f,
//                    0,
//                    2,
//                    20f,
//                    2*SIZE_SMALL,
//                    5*SIZE_SMALL
//            );
//            data.system.addScript(activeFleets);
            
//            addBeacon(mainSystem,OCISystemType.SENTINEL);
        }
        
        //MAJOR systems generation
        for (StarSystemAPI system : MAJOR) {
            StarSystemData data = computeSystemData(system);
            SectorEntityToken station = addBattlestations(data,KH_Entities.MEDIUM_STATION);
            populateNonMain(data);

            KH_StationFleetManager activeFleets = new KH_StationFleetManager(
                    station,
                    5f,
                    (int)(SIZE_MEDIUM/2),
                    SIZE_MEDIUM,
                    15f,
                    SIZE_MEDIUM,
                    2*SIZE_MEDIUM
            );
            data.system.addScript(activeFleets);
            enforceClones(station);
//            addBeacon(system,OCISystemType.BORDER_PATROL);
        }
        
        //MAIN systems generation
        StarSystemData data = computeSystemData(mainSystem);
        SectorEntityToken station = addBattlestations(data,KH_Entities.CENTRAL_STATION);
        populateMain(data);
        
        KH_StationFleetManager activeFleets = new KH_StationFleetManager(
                station,
                10f,
                (int)(SIZE_LARGE/2),
                SIZE_LARGE,
                10f,
                SIZE_LARGE,
                2*SIZE_LARGE
        );
        data.system.addScript(activeFleets);
        
        addBeacon(mainSystem,OCISystemType.CORE);

        if (DEBUG) System.out.println("Finished generating OCI systems\n\n\n\n\n");

    }
	
    //ADD BEACON
    public static CustomCampaignEntityAPI addBeacon(StarSystemAPI system, OCISystemType type) {

        SectorEntityToken anchor = system.getHyperspaceAnchor();
        List<SectorEntityToken> points = Global.getSector().getHyperspace().getEntities(JumpPointAPI.class);

        float minRange = 600;

        float closestRange = Float.MAX_VALUE;
        JumpPointAPI closestPoint = null;
        for (SectorEntityToken entity : points) {
            JumpPointAPI point = (JumpPointAPI) entity;

            if (point.getDestinations().isEmpty()) continue;

            JumpDestination dest = point.getDestinations().get(0);
            if (dest.getDestination().getContainingLocation() != system) continue;

            float dist = Misc.getDistance(anchor.getLocation(), point.getLocation());
            if (dist < minRange + point.getRadius()) continue;

            if (dist < closestRange) {
                closestPoint = point;
                closestRange = dist;
            }
        }

        CustomCampaignEntityAPI beacon = Global.getSector().getHyperspace().addCustomEntity(null, null, KH_Entities.BEACON, KH_Factions.OCI);

        if (closestPoint == null) {
            float orbitDays = minRange / (10f + StarSystemGenerator.random.nextFloat() * 5f);
            beacon.setCircularOrbitPointingDown(anchor, StarSystemGenerator.random.nextFloat() * 360f, minRange, orbitDays);
        } else {
            float angleOffset = 20f + StarSystemGenerator.random.nextFloat() * 20f;
            float angle = Misc.getAngleInDegrees(anchor.getLocation(), closestPoint.getLocation()) + angleOffset;
            float radius = closestRange;

            if (closestPoint.getOrbit() != null) {
                
                OrbitAPI orbit = Global.getFactory().createCircularOrbitPointingDown(anchor, angle, radius, 
                                closestPoint.getOrbit().getOrbitalPeriod()); 
                beacon.setOrbit(orbit);
            } else {
                Vector2f beaconLoc = Misc.getUnitVectorAtDegreeAngle(angle);
                beaconLoc.scale(radius);
                Vector2f.add(beaconLoc, anchor.getLocation(), beaconLoc);
                beacon.getLocation().set(beaconLoc);
            }
        }

        Color glowColor = new Color(0,50,255,255);
        Color pingColor = new Color(0,50,255,255);
        if (type == OCISystemType.BORDER_PATROL) {
            glowColor = new Color(0,100,255,255);
            pingColor = new Color(0,100,255,255);
        } else if (type == OCISystemType.CORE) {
            glowColor = new Color(0,150,255,255);
            pingColor = new Color(0,150,255,255);
        }
        Misc.setWarningBeaconColors(beacon, glowColor, pingColor);
        
        beacon.setTransponderOn(true);

        return beacon;
    }
    
    private void enforceClones(SectorEntityToken station){
        if(station.getMarket()==null)return;
        
        for(PersonAPI p : station.getMarket().getPeopleCopy()){
            p.getName().setGender(FullName.Gender.MALE);
//            p.setPortraitSprite("graphics/KH/portraits/KH_portrait_0"+MathUtils.getRandomNumberInRange(1, 6)+".png");
        }
    }
    
    //NON MAIN COLONIES
    public void populateNonMain(StarSystemData data) {
        if (DEBUG) System.out.println(" Generating secondary OCI system in " + data.system.getName());
        boolean special = data.isBlackHole() || data.isNebula() || data.isPulsar();
        
        if (special) {
                addResearchStations(data, 0.75f, 1, 1, createStringPicker(Entities.STATION_RESEARCH, 10f));
        }

        if (random.nextFloat() < 0.5f) return;

            if (!data.resourceRich.isEmpty()) {
                addMiningStations(data, 0.5f, 1, 1, createStringPicker(Entities.STATION_MINING, 10f));
            }

            if (!special && !data.habitable.isEmpty()) {
                // ruins on planet, or orbital station
                addHabCenters(data, 0.25f, 1, 1, createStringPicker(Entities.ORBITAL_HABITAT, 10f));
            }


        addShipGraveyard(data, 0.05f, 1, 1, createStringPicker(
                Factions.INDEPENDENT, 10f,
                Factions.SCAVENGERS, 8f,
                "OCI", 6f,
                Factions.PIRATES, 3f)
        );

        addDebrisFields(data, 0.25f, 1, 2);

        addDerelictShips(data, 0.5f, 0, 3, createStringPicker(
                Factions.INDEPENDENT, 10f,
                Factions.SCAVENGERS, 8f,
                "OCI", 6f,
                Factions.PIRATES, 3f)
        );

		addCaches(data, 0.25f, 0, 2, createStringPicker(
                        Entities.WEAPONS_CACHE, 2f,
                        Entities.WEAPONS_CACHE_SMALL, 5f,
                        "supply_cache", 4f,
                        "supply_cache_small", 10f,
                        "equipment_cache", 4f,
                        "equipment_cache_small", 10f)
                );

    }
        
    //MAIN COLONY
    public void populateMain(StarSystemData data) {

        if (DEBUG) System.out.println(" Generating OCI center in " + data.system.getName());

        int maxHabCenters = 1 + random.nextInt(3);

        HabitationLevel level = HabitationLevel.LOW;
        if (maxHabCenters == 2) level = HabitationLevel.MEDIUM;
        if (maxHabCenters >= 3) level = HabitationLevel.HIGH;

        addHabCenters(data, 1, maxHabCenters, maxHabCenters, createStringPicker(Entities.ORBITAL_HABITAT, 10f));

        // add various stations, orbiting entities, etc
        float probGate = 1f;
        float probRelay = 1f;
        float probMining = 0.5f;
        float probResearch = 0.25f;

        switch (level) {
        case HIGH:
            probGate = 0.75f;
            probRelay = 1f;
            break;
        case MEDIUM:
            probGate = 0.5f;
            probRelay = 0.75f;
            break;
        case LOW:
            probGate = 0.25f;
            probRelay = 0.5f;
            break;
        }

        addObjectives(data, probRelay);
        addInactiveGate(data, probGate, 0.5f, 0.5f, createStringPicker(
                Factions.TRITACHYON, 10f,
                Factions.HEGEMONY, 7f,
                "OCI", 6f,
                Factions.INDEPENDENT, 3f)
        );

        addShipGraveyard(data, 0.25f, 1, 1, createStringPicker(
                Factions.TRITACHYON, 10f,
                Factions.HEGEMONY, 7f,
                "OCI", 6f,
                Factions.INDEPENDENT, 3f)
        );

        addMiningStations(data, probMining, 1, 1, createStringPicker(Entities.STATION_MINING, 10f));

        addResearchStations(data, probResearch, 1, 1, createStringPicker(Entities.STATION_RESEARCH, 10f));

        addDebrisFields(data, 0.75f, 1, 5);

        addDerelictShips(data, 0.75f, 0, 7, createStringPicker(
                Factions.TRITACHYON, 10f, 
                Factions.HEGEMONY, 7f, 
                "OCI", 6f,
                Factions.INDEPENDENT, 3f)
        );

        addCaches(data, 0.75f, 0, 3, createStringPicker(
                Entities.WEAPONS_CACHE, 5f,
                Entities.WEAPONS_CACHE_SMALL, 5f,
                "supply_cache", 10f,
                "supply_cache_small", 10f,
                "equipment_cache", 10f,
                "equipment_cache_small", 10f)
        );
    }
    
    //ADD STATIONS
    public SectorEntityToken addBattlestations(StarSystemData data, String stationType) {
        SectorEntityToken result=null;

        if (DEBUG) System.out.println("Adding station to "+data.system.getName());

        //find location
        EntityLocation loc = pickCommonLocation(random, data.system, 200f, true, null);

        if (loc != null) {
            
            
            int size=3;
            List<String> industries= new ArrayList<>();
            String population=Conditions.POPULATION_3;
            String name="Central";
            String entityType="kh_station_large";
            
            switch (stationType){
                case KH_Entities.SMALL_STATION:
                    size = SIZE_SMALL;
                    name="Lighthouse";
                    entityType="kh_station_small";
                    population=Conditions.POPULATION_3;
                    industries.add(Industries.SPACEPORT);
                    industries.add(Industries.PATROLHQ);
                    industries.add(Industries.POPULATION);
                    industries.add(KH_Industries.SMALL_STATION);
                    industries.add(KH_Industries.RECYCLING);
                    break;
                case KH_Entities.MEDIUM_STATION:
                    size = SIZE_MEDIUM;
                    name="Gateway";
                    entityType="kh_station_medium";
                    population=Conditions.POPULATION_5;
                    industries.add(Industries.MEGAPORT);
                    industries.add(Industries.MILITARYBASE);
                    industries.add(Industries.POPULATION);
                    industries.add(Industries.HEAVYINDUSTRY);
                    industries.add(Industries.GROUNDDEFENSES);
                    industries.add(KH_Industries.MEDIUM_STATION);
                    industries.add(KH_Industries.RECYCLING);
                    break;
                case KH_Entities.CENTRAL_STATION:
                    size = SIZE_LARGE;
                    name="Central";
                    entityType="kh_station_large";
                    population=Conditions.POPULATION_7;
                    industries.add(Industries.MEGAPORT);
                    industries.add(Industries.HIGHCOMMAND);
                    industries.add(Industries.POPULATION);
                    industries.add(Industries.ORBITALWORKS);
                    industries.add(Industries.HEAVYBATTERIES);
                    industries.add(KH_Industries.CENTRAL_STATION);
                    industries.add(KH_Industries.RECYCLING);
            }
            
            MarketAPI market = Global.getFactory().createMarket("OCI_market_" + Misc.genUID(), name, size);
            
            market.setSize(size);
            market.setHidden(true);		
            market.setFactionId(KH_Factions.OCI);
            market.setSurveyLevel(SurveyLevel.FULL);
            market.addCondition(population);
		
            for(String s : industries){
                market.addIndustry(s);
            }
            
            market.addSubmarket(Submarkets.SUBMARKET_OPEN);
            if(stationType.equals(KH_Entities.CENTRAL_STATION)){
                market.addSubmarket(Submarkets.GENERIC_MILITARY);
            }
            market.addSubmarket(Submarkets.SUBMARKET_STORAGE);
		
//            market.getTariff().modifyFlat("default_tariff", market.getFaction().getTariffFraction());
            
            AddedEntity added = BaseThemeGenerator.addNonSalvageEntity(data.system, loc, entityType, KH_Factions.OCI);
		
            SectorEntityToken entity = added.entity;
            
            market.setName(name);
            entity.setName(name);
            
            BaseThemeGenerator.convertOrbitWithSpin(entity, -5f);

            market.setPrimaryEntity(entity);
            entity.setMarket(market);

            entity.setSensorProfile(1f);
            entity.setDiscoverable(true);
            entity.getDetectedRangeMod().modifyFlat("gen", 5000f);

            market.setEconGroup(KH_Factions.OCI);
            market.getMemoryWithoutUpdate().set(DecivTracker.NO_DECIV_KEY, true);
            
            market.setImmigrationClosed(true);
            
            market.reapplyIndustries();

            Global.getSector().getEconomy().addMarket(market, true);
            
            result=entity;
        }
        return result;
    }

    protected List<Constellation> getSortedAvailableConstellations(ThemeGenContext context, boolean emptyOk, final Vector2f sortFrom, List<Constellation> exclude) {
        List<Constellation> constellations = new ArrayList<>();
        for (Constellation c : context.constellations) {
            if (context.majorThemes.containsKey(c)) continue;
            if (!emptyOk && constellationIsEmpty(c)) continue;

            constellations.add(c);
        }

        if (exclude != null) {
            constellations.removeAll(exclude);
        }

        Collections.sort(constellations, new Comparator<Constellation>() {
            @Override
            public int compare(Constellation o1, Constellation o2) {
                float d1 = Misc.getDistance(o1.getLocation(), sortFrom);
                float d2 = Misc.getDistance(o2.getLocation(), sortFrom);
                return (int) Math.signum(d2 - d1);
            }
        });
        return constellations;
    }

    public static boolean constellationIsEmpty(Constellation c) {
        for (StarSystemAPI s : c.getSystems()) {
            if (!systemIsEmpty(s)) return false;
        }
        return true;
    }

    public static boolean systemIsEmpty(StarSystemAPI system) {
        for (PlanetAPI p : system.getPlanets()) {
            if (!p.isStar()) return false;
        }
        return true;
    }

    public List<StarSystemData> getSortedSystemsSuitedToBePopulated(List<StarSystemData> systems) {
        List<StarSystemData> result = new ArrayList<StarSystemData>();

        for (StarSystemData data : systems) {
            if (data.isBlackHole() || data.isNebula() || data.isPulsar()) continue;

            if (data.planets.size() >= 4 || data.habitable.size() >= 1) {
                result.add(data);
            }
        }

        Collections.sort(systems, new Comparator<StarSystemData>() {
            @Override
            public int compare(StarSystemData o1, StarSystemData o2) {
                float s1 = getMainCenterScore(o1);
                float s2 = getMainCenterScore(o2);
                return (int) Math.signum(s2 - s1);
            }
        });

        return result;
    }

    public float getMainCenterScore(StarSystemData data) {
        float total = 0f;
        total += data.planets.size() * 1f;
        total += data.habitable.size() * 2f;
        total += data.resourceRich.size() * 0.25f;
        return total;
    }

    @Override
    public int getOrder() {
        return 1500;
    }
}

















