package data.campaign.procgen.themes;

import java.util.Random;

import org.lwjgl.util.vector.Vector2f;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FleetAssignment;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import data.campaign.ids.KH_Factions;

public class KH_AssignmentAI implements EveryFrameScript {

    protected StarSystemAPI homeSystem;
    protected CampaignFleetAPI fleet;
    protected SectorEntityToken source;


    public KH_AssignmentAI(CampaignFleetAPI fleet, StarSystemAPI homeSystem, SectorEntityToken source) {
        this.fleet = fleet;
        this.homeSystem = homeSystem;
        this.source = source;

        giveInitialAssignments();
    }
	
    private void giveInitialAssignments() {
        boolean playerInSameLocation = fleet.getContainingLocation() == Global.getSector().getCurrentLocation();

        // launch from source if player is in-system, or sometimes
        if (playerInSameLocation && (float) Math.random() < 0.1f && source != null) {
            fleet.setLocation(source.getLocation().x, source.getLocation().y);
            fleet.addAssignment(FleetAssignment.ORBIT_PASSIVE, source, 3f + (float) Math.random() * 2f);
        } else {
            // start at random location
            SectorEntityToken target = pickEntityToGuard(new Random(), homeSystem, fleet);
            if (target != null) {
                    Vector2f loc = Misc.getPointAtRadius(target.getLocation(), target.getRadius() + 100f);
                    fleet.setLocation(loc.x, loc.y);
            } else {
                    Vector2f loc = Misc.getPointAtRadius(new Vector2f(), 5000f);
                    fleet.setLocation(loc.x, loc.y);
            }
        }
        
        transferAssignment();
    }
	
    protected void patrolAssignment() {
        boolean standDown = source != null && (float) Math.random() < 0.2f;
        if (!standDown) {
            SectorEntityToken target = pickEntityToGuard(new Random(), homeSystem, fleet);
            if (target != null) {
                float speed = Misc.getSpeedForBurnLevel(8);
                float dist = Misc.getDistance(fleet.getLocation(), target.getLocation());
                float seconds = dist / speed;
                float days = seconds / Global.getSector().getClock().getSecondsPerDay();
                days += 5f + 5f * (float) Math.random();
                fleet.addAssignment(FleetAssignment.PATROL_SYSTEM, target, days, "watching");
                return;
            } else {
                float days = 5f + 5f * (float) Math.random();
                fleet.addAssignment(FleetAssignment.PATROL_SYSTEM, null, days, "watching");
            }
        }
		
        if (source != null) {
            float dist = Misc.getDistance(fleet.getLocation(), source.getLocation());
            if (dist > 1000) {
                    fleet.addAssignment(FleetAssignment.PATROL_SYSTEM, source, 3f, "returning from patrol");
            } else {
                    fleet.addAssignment(FleetAssignment.ORBIT_PASSIVE, source, 3f + (float) Math.random() * 2f, "standing down");
                    fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, source, 5f);
            }
        }
    }
    
    protected void transferAssignment() {
        
//        fleet.addAbility("kh_slipstream");
        WeightedRandomPicker<SectorEntityToken> picker = new WeightedRandomPicker<>(new Random());
        for(MarketAPI m : Global.getSector().getEconomy().getMarketsInGroup(KH_Factions.OCI)){
            if(m.getPrimaryEntity()!=source){
                picker.add(m.getPrimaryEntity());
            }
        }
        SectorEntityToken target = picker.pick();
        
        if (target == null) return;
        
        fleet.addAssignment(FleetAssignment.DELIVER_RESOURCES, target, 90, "transfering ressources");
        fleet.addAssignment(FleetAssignment.ORBIT_PASSIVE, target, 3f + (float) Math.random() * 2f, "unloading");
        fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, target, 5f);
    }
    
    private SectorEntityToken pickEntityToGuard(Random random, StarSystemAPI system, CampaignFleetAPI fleet) {
            WeightedRandomPicker<SectorEntityToken> picker = new WeightedRandomPicker<SectorEntityToken>(random);

            for (SectorEntityToken entity : system.getEntitiesWithTag(Tags.SALVAGEABLE)) {
                    float w = 1f;
                    if (entity.hasTag(Tags.NEUTRINO_HIGH)) w = 3f;
                    if (entity.hasTag(Tags.NEUTRINO_LOW)) w = 0.33f;
                    picker.add(entity, w);
            }

            for (SectorEntityToken entity : system.getJumpPoints()) {
                    picker.add(entity, 1f);
            }

            return picker.pick();
    }
    
    @Override
    public void advance(float amount) {
        if (fleet.getCurrentAssignment() == null) {
            patrolAssignment();
        }
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public boolean runWhilePaused() {
        return false;
    }
}










