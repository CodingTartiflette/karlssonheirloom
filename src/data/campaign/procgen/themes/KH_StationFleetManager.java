package data.campaign.procgen.themes;

import java.util.Random;

import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.CampaignEventListener.FleetDespawnReason;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.CargoStackAPI;
import com.fs.starfarer.api.campaign.FactionAPI.ShipPickMode;
import com.fs.starfarer.api.campaign.FleetEncounterContextPlugin;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.characters.FullName;
import com.fs.starfarer.api.characters.OfficerDataAPI;
import com.fs.starfarer.api.combat.BattleCreationContext;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.FleetEncounterContext;
import com.fs.starfarer.api.impl.campaign.FleetInteractionDialogPluginImpl;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV3;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParamsV3;
import com.fs.starfarer.api.impl.campaign.fleets.SourceBasedFleetManager;
import com.fs.starfarer.api.impl.campaign.ids.Abilities;
import com.fs.starfarer.api.impl.campaign.ids.Drops;
import com.fs.starfarer.api.impl.campaign.ids.FleetTypes;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.impl.campaign.procgen.SalvageEntityGenDataSpec;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.SalvageEntity;
import com.fs.starfarer.api.util.Misc;
import java.util.ArrayList;
import java.util.List;

public class KH_StationFleetManager extends SourceBasedFleetManager {

    protected int minPts;
    protected int maxPts;
    protected int totalLost;

    public KH_StationFleetManager(SectorEntityToken source, float thresholdLY, int minFleets, int maxFleets, float respawnDelay, 
                                                                      int minPts, int maxPts) {
        super(source, thresholdLY, minFleets, maxFleets, respawnDelay);
        this.minPts = minPts;
        this.maxPts = maxPts;
        this.source = source;
    }

    @Override
    protected CampaignFleetAPI spawnFleet() {
        Random random = new Random();

        int combatPoints = minPts + random.nextInt(maxPts - minPts + 1);

        int bonus = totalLost * 4;
        if (bonus > maxPts) bonus = maxPts;

        combatPoints += bonus;

        String type = FleetTypes.PATROL_SMALL;
        if (combatPoints > 8) type = FleetTypes.PATROL_MEDIUM;
        if (combatPoints > 16) type = FleetTypes.PATROL_LARGE;

        combatPoints *= 8f;

        FleetParamsV3 params = new FleetParamsV3(
                source.getMarket(),
                source.getLocationInHyperspace(),
                "OCI",
                1f,
                type,
                combatPoints, // combatPts
                0f, // freighterPts 
                0f, // tankerPts
                0f, // transportPts
                0f, // linerPts
                0f, // utilityPts
                1f // qualityMod
        );
        params.officerNumberBonus = 5;
        params.ignoreMarketFleetSizeMult = true;
        params.modeOverride = ShipPickMode.PRIORITY_THEN_ALL;

        CampaignFleetAPI fleet = FleetFactoryV3.createFleet(params);
        if (fleet == null || fleet.getFlagship()==null) return null;

        LocationAPI location = source.getContainingLocation();
        location.addEntity(fleet);

        initOCIFleetProperties(random, fleet);
        fleet.setName("Transfer Fleet");

        fleet.setLocation(source.getLocation().x, source.getLocation().y);
        fleet.setFacing(random.nextFloat() * 360f);
        
        fleet.getCommander().getName().setGender(FullName.Gender.MALE);
        for(OfficerDataAPI o : fleet.getFleetData().getOfficersCopy()){
            o.getPerson().getName().setGender(FullName.Gender.MALE);
        }

        fleet.addScript(new KH_AssignmentAI(fleet, (StarSystemAPI) source.getContainingLocation(), source));

        return fleet;
    }


    @Override
    public void reportFleetDespawnedToListener(CampaignFleetAPI fleet, FleetDespawnReason reason, Object param) {
        super.reportFleetDespawnedToListener(fleet, reason, param);
        if (reason == FleetDespawnReason.DESTROYED_BY_BATTLE) {
            totalLost++;
        }
    }
    
    public static void initOCIFleetProperties(Random random, CampaignFleetAPI fleet) {
        if (random == null) random = new Random();

        fleet.removeAbility(Abilities.SUSTAINED_BURN);
        fleet.addAbility("kh_slipstream");
        fleet.removeAbility(Abilities.SENSOR_BURST);
        fleet.removeAbility(Abilities.GO_DARK);

        // to make sure they attack the player on sight when player's transponder is off
//        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_SAW_PLAYER_WITH_TRANSPONDER_ON, true);
//        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_PATROL_FLEET, true);
//        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_ALLOW_LONG_PURSUIT, true);
//
//        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_NO_JUMP, true);
        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_MAKE_NON_AGGRESSIVE, true);
        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_TRADE_FLEET, true);
        
        addOCIInteractionConfig(fleet);
        addOCIAICoreDrops(random, fleet, 1f);
    }
	
    public static void addOCIInteractionConfig(CampaignFleetAPI fleet) {
        fleet.getMemoryWithoutUpdate().set(MemFlags.FLEET_INTERACTION_DIALOG_CONFIG_OVERRIDE_GEN, new OCIFleetInteractionConfigGen());		
    }
        
    public static class OCIFleetInteractionConfigGen implements FleetInteractionDialogPluginImpl.FIDConfigGen {
        @Override
        public FleetInteractionDialogPluginImpl.FIDConfig createConfig() {
            FleetInteractionDialogPluginImpl.FIDConfig config = new FleetInteractionDialogPluginImpl.FIDConfig();
            config.showTransponderStatus = false;
            config.delegate = new FleetInteractionDialogPluginImpl.FIDDelegate() {
                @Override
                public void postPlayerSalvageGeneration(InteractionDialogAPI dialog, FleetEncounterContext context, CargoAPI salvage) {
                    if (!(dialog.getInteractionTarget() instanceof CampaignFleetAPI)) return;

                    CampaignFleetAPI fleet = (CampaignFleetAPI) dialog.getInteractionTarget();

                    FleetEncounterContextPlugin.DataForEncounterSide data = context.getDataFor(fleet);
                    List<FleetMemberAPI> losses = new ArrayList<>();
                    for (FleetEncounterContextPlugin.FleetMemberData fmd : data.getOwnCasualties()) {
                        losses.add(fmd.getMember());
                    }

                    List<SalvageEntityGenDataSpec.DropData> dropRandom = new ArrayList<>();

                    int [] counts = new int[3];
                    String [] groups = new String [] {Drops.AI_CORES1, Drops.AI_CORES2, Drops.AI_CORES3};
                    for (FleetMemberAPI member : losses) {
                        if (member.isStation()) {
                                counts[2] += 10;
                        }

                        if (member.isCapital()) {
                            counts[2] += 2;
                        } else if (member.isCruiser()) {
                            counts[2] += 1;
                        } else if (member.isDestroyer()) {
                            counts[1] += 1;
                        } else if (member.isFrigate()) {
                            counts[0] += 1;
                        }
                    }

                    for (int i = 0; i < counts.length; i++) {
                        int count = counts[i];
                        if (count <= 0) continue;

                        SalvageEntityGenDataSpec.DropData d = new SalvageEntityGenDataSpec.DropData();
                        d.group = groups[i];
                        d.chances = (int) Math.ceil(count * 1f);
                        dropRandom.add(d);
                    }

                    Random salvageRandom = new Random(Misc.getSalvageSeed(fleet));
                    CargoAPI extra = SalvageEntity.generateSalvage(salvageRandom, 1f, 1f, 1f, 1f, null, dropRandom);
                    for (CargoStackAPI stack : extra.getStacksCopy()) {
                        salvage.addFromStack(stack);
                    }
                }
                @Override
                public void notifyLeave(InteractionDialogAPI dialog) {
                }
                @Override
                public void battleContextCreated(InteractionDialogAPI dialog, BattleCreationContext bcc) {
                    bcc.aiRetreatAllowed = true;
                    bcc.objectivesAllowed = false;
                }
            };
            return config;
        }
    }
	
        
    //EXTRA CORES LOOT
    public static void addOCIAICoreDrops(Random random, CampaignFleetAPI fleet, float mult) {
        long salvageSeed = random.nextLong();
        fleet.getMemoryWithoutUpdate().set(MemFlags.SALVAGE_SEED, salvageSeed);

        int [] counts = new int[3];
        String [] groups = new String [] {Drops.AI_CORES1, Drops.AI_CORES2, Drops.AI_CORES3};
        for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
            if (member.isCapital()) {
                counts[2] += 2;
            } else if (member.isCruiser()) {
                counts[2] += 1;
            } else if (member.isDestroyer()) {
                counts[1] += 1;
            } else if (member.isFrigate()) {
                counts[0] += 1;
            }
        }

        if (fleet.isStationMode()) {
            counts[2] += 10;
        }

        for (int i = 0; i < counts.length; i++) {
            int count = counts[i];
            if (count <= 0) continue;

            SalvageEntityGenDataSpec.DropData d = new SalvageEntityGenDataSpec.DropData();
            d.group = groups[i];
            d.chances = (int) Math.ceil(count * mult);
            fleet.addDropRandom(d);
        }
    }
}
