package data.campaign.ids;

public class KH_Entities {
    public static final String SMALL_STATION = "kh_station_small_variant";
    public static final String MEDIUM_STATION = "kh_station_medium_variant";
    public static final String CENTRAL_STATION = "kh_station_large_variant";
    public static final String BEACON = "kh_beacon";
}