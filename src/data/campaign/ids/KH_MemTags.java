package data.campaign.ids;

public class KH_MemTags
{
    // This memory tag is set on the OCI faction when player encounters an OCI fleet or station
    public static final String MEM_ENCOUNTERED_OCI = "$kh_playerEncounteredOCI";
    public static final String MEM_SMUGGLE_MISSION_ACTIVE = "$kh_smuggleMissionActive";
    public static final String MEM_SMUGGLER_MISSION_AGENT = "$kh_smuggleMissionAgent";
}
