package data.campaign.ids;

public class KH_Industries {
    public static final String SMALL_STATION = "kh_orbitalstation";
    public static final String MEDIUM_STATION = "kh_battlestation";
    public static final String CENTRAL_STATION = "kh_starfortress";
    public static final String RECYCLING = "kh_recycling";
}