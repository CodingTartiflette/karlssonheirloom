package data.campaign.ids;

public class KH_Tags {
    public static final String THEME_OCI = "theme_oci";
    public static final String THEME_OCI_SMALL = "theme_oci_small";
    public static final String THEME_OCI_LARGE = "theme_oci_large";
    public static final String THEME_OCI_CORE = "theme_oci_core";
}