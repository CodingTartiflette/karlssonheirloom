package data.campaign.abilities;

import org.lwjgl.util.vector.Vector2f;
import com.fs.starfarer.api.campaign.ai.FleetAIFlags;
import com.fs.starfarer.api.campaign.ai.ModularFleetAIAPI;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.impl.campaign.abilities.ai.BaseAbilityAI;
import com.fs.starfarer.api.util.IntervalUtil;
import org.lazywizard.lazylib.MathUtils;
import org.lazywizard.lazylib.VectorUtils;

public class kh_slipstreamAI extends BaseAbilityAI {
    
    public static final float AI_FREQUENCY_MULT = 1f;
	
    private IntervalUtil interval = new IntervalUtil(0.05f, 0.15f);

    @Override
    public void advance(float days) {
        //timer
        float mult=1;
        if(ability.isActiveOrInProgress()){
            //frequent checks when using the ability
            mult=10;
        }
        interval.advance(days*mult);
        if (!interval.intervalElapsed()) return;
        
        //only works in hyperspace
        if(!fleet.isInHyperspace()){
            if(ability.isActiveOrInProgress()){
                ability.deactivate();
            }
            return;
        }
        
        //keep flags up
        MemoryAPI mem = fleet.getMemoryWithoutUpdate();
        if (ability.isActiveOrInProgress()) {
            mem.set(FleetAIFlags.HAS_SPEED_BONUS, true, 0.2f);
            mem.set(FleetAIFlags.HAS_HIGHER_DETECTABILITY, true, 0.2f);
        }
		
        if (fleet.getAI() instanceof ModularFleetAIAPI) {
            ModularFleetAIAPI ai = (ModularFleetAIAPI) fleet.getAI();
            if (ai.getTacticalModule().isMaintainingContact()) {
                if (ability.isActive()) ability.deactivate();
                return;
            }
        }
		
        //do not trigger when stealthy
        if (mem.getBoolean(FleetAIFlags.HAS_LOWER_DETECTABILITY) && !ability.isActive()) {
            return;
        }
		
        if (fleet.getAI() != null && fleet.getAI().getCurrentAssignment() != null) {
            
            Vector2f travelDest = mem.getVector2f(FleetAIFlags.TRAVEL_DEST);
            if (travelDest != null) {
                
                if(!ability.isActiveOrInProgress()){
                    //ability off, should it be used?
                    if(
                            !MathUtils.isWithinRange(travelDest, fleet.getLocation(), 1000) &&
                            Math.abs(MathUtils.getShortestRotation(VectorUtils.getFacing(fleet.getVelocity()), VectorUtils.getAngle(fleet.getLocation(), travelDest)))>10
                            ){
                        if(ability.isUsable()){
                            ability.activate();
                        }
                    }
                } else {
                    if(MathUtils.isWithinRange(travelDest, fleet.getLocation(), 100)){
                        //ability in use, is the fleet at the destination?
                        ability.deactivate();
                        return;
                    }
                    //ability in use, is the fleet off course?
                    if(ability.getProgressFraction()==1 && Math.abs(MathUtils.getShortestRotation(VectorUtils.getFacing(fleet.getVelocity()), VectorUtils.getAngle(fleet.getLocation(), travelDest)))>10){
                        ability.deactivate();
                    }
                }
            } else if(ability.isActiveOrInProgress()){
                //deactivate if there is no destination
                ability.deactivate();
            }
        }
    }
}