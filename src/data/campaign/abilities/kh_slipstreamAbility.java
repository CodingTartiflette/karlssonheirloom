package data.campaign.abilities;

import java.awt.Color;
import org.lwjgl.util.vector.Vector2f;
import com.fs.starfarer.api.campaign.BattleAPI;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberViewAPI;
import com.fs.starfarer.api.impl.campaign.abilities.BaseToggleAbility;
import com.fs.starfarer.api.ui.LabelAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;
import org.lazywizard.lazylib.MathUtils;

public class kh_slipstreamAbility extends BaseToggleAbility {
    
    private final float DETECTABILITY_PERCENT = 300f;
    private final float FUEL_CONSUMPTION = 0f;

//    private static Map<HullSize,Integer> COST = new HashMap<>();
//    static {
//        COST.put(HullSize.DEFAULT, 100);
//        COST.put(HullSize.FRIGATE, 5);
//        COST.put(HullSize.DESTROYER, 15);
//        COST.put(HullSize.CRUISER, 30);
//        COST.put(HullSize.CAPITAL_SHIP, 50);
//    }
    
    @Override
    protected String getActivationText() {
        return "Engaging Slipstream Drive";
    }
    
    @Override
    protected String getDeactivationText() {
            return "Disengaging Slipstream Drive";
    }
    
    //IS USABLE
    @Override
    public boolean isUsable() {
        CampaignFleetAPI fleet = getFleet();
        if(fleet==null){return false;}
        
        Boolean carrier=false;
        for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
            if(!member.isMothballed() && member.getHullId().equals("kh_integrity")){
                carrier=true;
            }
        }

        return super.isUsable() &&
                carrier && //requires carrier ship
                (isActive()|| 
                getFleet().isInHyperspace()&& //hyperspace only ability
                (
                    getFleet().isAIMode() || 
                        (computeFuelCost() <= getFleet().getCargo().getFuel()&&
                        computeSupplyCost() <= getFleet().getCargo().getSupplies()
                    )
                ));
    }
    
    //FUEL COST CHECK
    private float computeFuelCost() {
        CampaignFleetAPI fleet = getFleet();
        if (fleet == null) return 0f;
        float cost = 0f;
        for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
            if(member.isCapital()){
//                cost+=COST.get(HullSize.CAPITAL_SHIP);
                cost+=160;
            } else 
            if(member.isCruiser()){
//                cost+=COST.get(HullSize.CRUISER);
                cost+=80;
            } else 
            if(member.isDestroyer()){
//                cost+=COST.get(HullSize.DESTROYER);
                cost+=40;
            } else 
            if(member.isFrigate()){
//                cost+=COST.get(HullSize.FRIGATE);
                cost+=20;
            }
        }
        return cost;
    }
    
    //SUPPLIES COST CHECK
    private float computeSupplyCost() {
        CampaignFleetAPI fleet = getFleet();
        if (fleet == null) return 0f;
        float cost = 0f;
        for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
            if(member.isCapital()){
//                cost+=COST.get(HullSize.CAPITAL_SHIP);
                cost+=55;
            } else 
            if(member.isCruiser()){
//                cost+=COST.get(HullSize.CRUISER);
                cost+=35;
            } else 
            if(member.isDestroyer()){
//                cost+=COST.get(HullSize.DESTROYER);
                cost+=20;
            } else 
            if(member.isFrigate()){
//                cost+=COST.get(HullSize.FRIGATE);
                cost+=10;
            }
        }
        return cost;
    }

    //TURN ON
    @Override
    protected void activateImpl() {
        CampaignFleetAPI fleet = getFleet();
        if (fleet == null) return;

        //nullify fuel usage
        fleet.getStats().getFuelUseHyperMult().modifyMult(getModId(), FUEL_CONSUMPTION);
        
        //pay thine
        float cost = computeFuelCost();
        fleet.getCargo().removeFuel(cost);
        
        cost = computeSupplyCost();
        fleet.getCargo().removeSupplies(cost);
    }	
    
    //TURN OFF
    @Override
    protected void deactivateImpl() {
        CampaignFleetAPI fleet = getFleet();
        if (fleet == null) return;    
        fleet.setVelocity(0, 0);
        cleanupImpl();
    }
	
    private Vector2f dir=new Vector2f();

    @Override
    protected void applyEffect(float amount, float level) {
        CampaignFleetAPI fleet = getFleet();
        if (fleet == null) return;

        if (level > 0 && 
                !fleet.isAIMode() && 
                fleet.getContainingLocation() != null &&
                !fleet.getContainingLocation().isHyperspace() //turn off in normal space
                ) {
            deactivate();
            return;
        }

        fleet.getStats().getDetectedRangeMod().modifyPercent(getModId(), DETECTABILITY_PERCENT * level, "Slipstream Drive");

        if(level<1){
            
//            ping+=amount;
//            if(ping>0.05f){
//                ping=0f;
//                float range = fleet.getRadius()+300*(1-level);
//                
//                CampaignPingSpec custom = new CampaignPingSpec();
//                custom.setUseFactionColor(true);
//                custom.setWidth(7);
//                custom.setMinRange(range - 100f);
//                custom.setRange(200);
//                custom.setDuration(2f);
//                custom.setAlphaMult(0.25f);
//                custom.setInFraction(0.2f);
//                custom.setNum(1);
//
//                Global.getSector().addPing(fleet, custom);
//            }
            
            fleet.getStats().getAccelerationMult().modifyMult(getModId(), 1f - level);
            
            //set the movement vector
            if(fleet.getCurrBurnLevel()>0){
                dir = MathUtils.getPoint(
                        new Vector2f(),
                        800,
                        Misc.getDesiredMoveDir(fleet));
            } else {dir=new Vector2f();}
            return;
        }

        if(dir==new Vector2f()){
            deactivate();
            return;
        }

        //MAX WARP!        
        fleet.setVelocity(dir.x,dir.y);

        for (FleetMemberViewAPI view : fleet.getViews()) {
            view.getContrailColor().shift(getModId(), new Color(128,128,128,128), 1f, 1f, 0.5f * level);
            view.getEngineGlowSizeMult().shift(getModId(), 1.5f, 1f, 1f, 1f * level);
            view.getEngineHeightMult().shift(getModId(), 3f, 1f, 1f, 1f * level);
//            view.getEngineWidthMult().shift(getModId(), 0.5f, 1f, 1f, 1f * level);

            float durIn = 1f;
            float durOut = 5f;
            float intensity = 2f;
            float sizeNormal = 0.05f;

            view.getWindEffectDirX().shift(getModId(), -dir.x * sizeNormal, durIn, durOut, 1f);
            view.getWindEffectDirY().shift(getModId(), -dir.y * sizeNormal, durIn, durOut, 1f);
            view.getWindEffectColor().shift(getModId(), new Color(64,64,128,128), durIn, durOut, intensity);
        }

        if (level <= 0) {
            cleanupImpl();
        }
    }
	
    @Override
    protected void cleanupImpl() {
        dir=new Vector2f();
        
        CampaignFleetAPI fleet = getFleet();
        if (fleet == null) return;
        fleet.getStats().getDetectedRangeMod().unmodify(getModId());
        fleet.getStats().getFleetwideMaxBurnMod().unmodify(getModId());
        fleet.getStats().getAccelerationMult().unmodify(getModId());
        
        //reset fuel usage
        fleet.getStats().getFuelUseHyperMult().unmodify(getModId());
	fleet.getStats().getAccelerationMult().unmodify(getModId());
    }
	
    @Override
    public boolean showProgressIndicator() {
        return super.showProgressIndicator();
    }
	
    @Override
    public boolean showActiveIndicator() {
        return isActive();
    }

    @Override
    public void createTooltip(TooltipMakerAPI tooltip, boolean expanded) {
        CampaignFleetAPI fleet = getFleet();
        if (fleet == null) return;
        Color gray = Misc.getGrayColor();
        Color highlight = Misc.getHighlightColor();
        Color bad = Misc.getNegativeHighlightColor();

        String status = " (off)";
        if (turnedOn) {
                status = " (on)";
        }

        LabelAPI title = tooltip.addTitle("Slipstream Drive" + status);
        title.highlightLast(status);
        title.setHighlightColor(gray);

        float fuelCost = computeFuelCost();
        float supplyCost = computeSupplyCost();
        float pad = 10f;

        tooltip.addPara("Toggle the fleet-wide Hyperspace engine of an Integrity-class capital ship. " +
                        "The fleet will be carried forward at high speed but loses its ability to steer. ", pad);

        tooltip.addPara("Intantly drives the fleet forward at %s burn rate, " +
                        "Reduce the fuel consumption to %s but has a high activation cost." +
                        "Also increases the range at which the fleet can be detected by %s.", pad,
                        highlight,
                        "40+",
                        ""+(int) FUEL_CONSUMPTION,
                        "" + (int)(DETECTABILITY_PERCENT) + "%"
        );
        
        tooltip.addPara("Activating the Slipstream drive will consume %s fuel and %s supplies.", pad, 
                                highlight,
                                Misc.getRoundedValueMaxOneAfterDecimal(fuelCost),
                                Misc.getRoundedValueMaxOneAfterDecimal(supplyCost)
        );
		
		
        if (fuelCost > fleet.getCargo().getFuel()) {
            tooltip.addPara("Not enough fuel.", bad, pad);
        }
        if (supplyCost > fleet.getCargo().getSupplies()) {
            tooltip.addPara("Not enough supplies.", bad, pad);
        }
        if (!fleet.isInHyperspace()) {
            tooltip.addPara("Not in Hyperspace.", bad, pad);
        }
        Boolean carrier=false;
        for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
            if(!member.isMothballed() && member.getHullId().equals("kh_integrity")){
                carrier=true;
            }
        }
        if(!carrier){
            tooltip.addPara("No Integrity-class Capital-ship in the fleet.", bad, pad);
        }
        
        addIncompatibleToTooltip(tooltip, expanded);
    }

    @Override
    public boolean hasTooltip() {
        return true;
    }
	
    @Override
    public void fleetLeftBattle(BattleAPI battle, boolean engagedInHostilities) {
        deactivate();
    }

    @Override
    public void fleetJoinedBattle(BattleAPI battle) {
        deactivate();
    }
}